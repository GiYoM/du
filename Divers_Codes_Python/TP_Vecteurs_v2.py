# -*- coding: utf-8 -*-
"""
Created on Sat 27 Apr 2019

@author: gconnan
"""
from math import sqrt
from dataclasses import dataclass


@dataclass
class Point:
    """
    Un point est caractérisé par ses coordonnées x et y
    """
    x: float
    y: float

    def alignes(self, B, C) -> bool:
        """
        A, B, C sont alignés ssi les vecteurs AB et AC sont colinéaires
        
        >>> A, B, C = Point(1, 3), Point(2,5), Point(3,7)
        >>> A.alignes(B, C)
        True
        """
        AB = Vecteur(self, B)
        AC = Vecteur(self, C)
        return AB.colineaire(AC)


class Vecteur:
    """
    Un vecteur est caractérisé par ses coordonnées x et y
    On peut le définir à partir de ses coordonnées ou de deux points
    """
    def __init__(self, arg1, arg2) -> None:
        if isinstance(arg1, Point):
            self.x = arg2.x - arg1.x
            self.y = arg2.y - arg1.y
        elif isinstance(arg1, float) or isinstance(arg1, int):
            self.x = arg1
            self.y = arg2

    def __repr__(self) -> str:
        return "Vecteur(x = {}, y = {})".format(self.x, self.y)

    def norme(self) -> float:
        """
        Calcule la norme d'un vecteur v
        Par exemple :
        
        >>> Vecteur(3,4).norme()
        5.0
        
        En effet, ||v|| = Racine(absisse de v au carré + ordo de v au carré)
        """
        return sqrt(self.x**2 + self.y**2)

    def colineaire(self, other) -> bool:
        """
        Produits en croix égaux
        
        >>> Vecteur(1, 2).colineaire(Vecteur(10, 20))
        True
        """
        return self.x * other.y == self.y * other.x


class Droite:
    """
    Une droite est définie par deux points ou un point et un vecteur
    ou une équation cartésienne donnée par un triplet (a, b, c)
    telle que l'équation soit ax + by + c = 0
    """

    def __init__(self, arg1, arg2=None) -> None:
        if isinstance(arg1, tuple):
            a, b, c = arg1
            self.vec: Vecteur = Vecteur(-b, a)
        else:
            if isinstance(arg2, Point):
                self.vec: Vecteur = Vecteur(arg1, arg2)
            else:
                self.vec: Vecteur = arg2
            a, b = self.vec.y, -self.vec.x
            c = -a*arg1.x - b*arg1.y
        self.eq = "{}*x + ({}*y) + ({}) == 0".format(a, b, c)

    def __repr__(self) -> str:
        return "Droite({}, {})".format(self.eq, self.vec)

    def est_verticale(self) -> bool:
        """
        Teste si une droite est verticale

        >>> D =  Droite(Point(1, 3), Point(2, 5))
        >>> D.est_vecticale()
        False
        >>> D =  Droite(Point(1, 3), Point(1, 5))
        >>> D.est_vecticale()
        True
        """
        return self.vec.x == 0
        
    def coeff_dir(self) -> float:
        """
        Calcule le coefficient directeur de (AB) sous forme de flottant
        
        >>> Droite(Point(1,3), Point(2,5)).coeff_dir()
        2.0
        
        """
        assert not self.est_verticale(), "La droite %s est verticale" % (str(self))
        return self.vec.y / self.vec.x

    def vec_dir_normalisé(self) -> Vecteur:
        """
        Détermine, s'il existe, le vecteur directeur de la droite (AB)
        d'abscisse 1
        
        >>> Droite(Point(1, 3), Point(2, 5)).vec_dir_normalisé()
        Vecteur(x = 1, y = 2.0)
        >>> Droite(Point(1, 3), Point(1, 5)).vec_dir_normalisé()
        Vecteur(x = 0, y = 1)
        """
        if self.est_verticale():
            return Vecteur(0,1)
        return Vecteur(1, self.coeff_dir())

        def contient_point(self, A: Point) -> bool:
            """
            >>> A, B = Point(1, 3), Point(2, 5)
            >>> d = droite(A, B)
            >>> d.contient_point(A)
            True
            """
            x, y = A.x, A.y
            return eval(self.eq)


if __name__ == "__main__":
    import doctest
    doctest.testmod()
