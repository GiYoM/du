

## dis python

Considérons une fonction toute simple :

```python
def truc():
    a = 2
    b = 3
    return a * b
```

On peut obtenir son bytecode :

```python
In [13]: truc.__code__.co_code
Out[31]: b'd\x01}\x00d\x02}\x01|\x00|\x01\x14\x00S\x00'
```

Bof...Importons la bibliothèque `dis` :

```python
In [33]: import dis

In [35]: dis.dis(truc)
  2           0 LOAD_CONST               1 (2)
              2 STORE_FAST               0 (a)

  3           4 LOAD_CONST               2 (3)
              6 STORE_FAST               1 (b)

  4           8 LOAD_FAST                0 (a)
             10 LOAD_FAST                1 (b)
             12 BINARY_MULTIPLY
             14 RETURN_VALUE
```


Essayons un branchement :

```python
def branch(n):
    if n % 2 == 1:
        print('impair')
    elif n == 0:
        print('nul')
    else:
        return "pair"
```

on obtient :

```python
In [53]: dis.dis(branch)
  8           0 LOAD_FAST                0 (n)
              2 LOAD_CONST               1 (2)
              4 BINARY_MODULO
              6 LOAD_CONST               2 (1)
              8 COMPARE_OP               2 (==)
             10 POP_JUMP_IF_FALSE       22

  9          12 LOAD_GLOBAL              0 (print)
             14 LOAD_CONST               3 ('impair')
             16 CALL_FUNCTION            1
             18 POP_TOP
             20 JUMP_FORWARD            22 (to 44)

 10     >>   22 LOAD_FAST                0 (n)
             24 LOAD_CONST               4 (0)
             26 COMPARE_OP               2 (==)
             28 POP_JUMP_IF_FALSE       40

 11          30 LOAD_GLOBAL              0 (print)
             32 LOAD_CONST               5 ('nul')
             34 CALL_FUNCTION            1
             36 POP_TOP
             38 JUMP_FORWARD             4 (to 44)

 13     >>   40 LOAD_CONST               6 ('pair')
             42 RETURN_VALUE
        >>   44 LOAD_CONST               0 (None)
             46 RETURN_VALUE
```

Testez aussi le branchement imbriqué au lieu de cette version plate.

Comparez la création d'une liste par une boucle `for` avec sa version par compréhension.

	  
## Code assembleur

Chaque processeur  a son  langage assembleur.  Un langage  assez simple  et bien
documenté est  celui des  processeurs MIPS qui  ont été  principalement utilisés
dans  des   systèmes  embarqués.  Vous   pouvez  installer  le   simulteur  MARS
(http://courses.missouristate.edu/KenVollmar/MARS/) si vous disposez de `java`.

Nous  nous   contenterons  dans  cet   exercice  d'un  assembleur  en   ligne  :
https://alanhogan.com/asu/assembler.php  et nous  pourrons l'exécuter  sur cette
page: https://alanhogan.com/asu/simulator.php.

### Hello Dave

Voici par exemple un code permettant d'afficher "*Hello Dave*":

```asm
.text   # Indique que ce qui suit est le texte d'un programme à assembler
main:
	li $v0, 4 # Charge 4 dans $v0 ce qui indique 
	          # qu'il faudra afficher une chaine
	la $a0, salutation # Met dans $a0 l'adresse de la salutation
			     	   # salutation est le drapeau repérant la donnée
			 	       # qui sera introduite (ici ligne 16)
	syscall   # Execute l'action codée dans $v0		
	
	li $v0, 10  # 10 code la terminaison du programme
	syscall     # Exécute l'action codée dans $v0 : fin du programme
	
.data     # Les données du programme stockées dans la 1ere
          # adresse disponible
 salutation: .asciiz "Hello Dave" 
```


Sur MARS, on peut  voir ce que contiennent les registres et  la mémoire à chaque
étape de l'exécution.


Sur  la  version en  ligne  on  récupère dans  un  premier  temps la  sortie  de
l'assembleur :

```
00400000: <main> ; <input:2> main:
00400000: 20020004 ; <input:3> li $v0, 4 # charge 4 dans $v0 ce qui indique
00400004: 240400c0 ; <input:5> la $a0, salutation # met dans $a0 l'adresse de la salutation
00400008: 00042400 ; <input:5> la $a0, salutation # met dans $a0 l'adresse de la salutation
0040000c: 24840000 ; <input:5> la $a0, salutation # met dans $a0 l'adresse de la salutation
00400010: 0000000c ; <input:8> syscall # execute l'action codée dans $v0
00400014: 2002000a ; <input:10> li $v0, 10 # 10 code la terminaison du programme
00400018: 0000000c ; <input:11> syscall # exécute l'action codée dans $v0 : fin du programme

; DATA IN MEMORY
; salutation
00c00000: 48656c6c ; Hell
00c00004: 6f204461 ; o Da
00c00008: 76650000 ; ve
```


et on la copie dans la fenêtre de la seconde page web pour obtenir:

![MIPS online](./IMG/mipsphp.png)

### Doublement d'un nombre

Voici un programme plus sophistiqué interactif : on demande un nombre et on fait
afficher son double:

```asm
# 2e programme ; doubler un nombre
.text    
  main:
        # demande d'entrer un nombre
	li $v0, 4               
	la $a0, lecture
	syscall	
	# Lit le nombre et le stocke dans $s0
	li $v0, 5  # le code 5 correspond à la lecture d'une donnée
	syscall
	move $s0, $v0
	# double le nombre
	 # Plus généralement add Rr, R1, R2 stocke dans le registre Rr la somme 
	 # des contenus des registres R1 et R2
	add $s0, $s0, $s0
	# Affiche le texte d'introduction du résultat
	li $v0, 4
	la $a0, affichage
	syscall
	# Affiche le nombre obtenu
	li $v0, 1
	move $a0, $s0
	syscall
	# termine le programme
	li $v0, 10  # 10 code la terminaison du programme
	syscall      # exécute l'action codée dans $v0 : fin du programme
	
.data    
  lecture: .asciiz "Entrez un nombre : \n"
  affichage: .asciiz "\nLe double du nombre entré est :"  
```

et on obtient bien:

```
Entrez un nombre : 
12345

Le double du nombre entré est :24690
-- program is finished running --
```


### Somme de deux nombres

**À vous de jouer !** Proposez un  code assembleur qui demande la saisie de deux
entiers et renvoie leur somme.





<!-- 
  ## CORRECTION ASSEMBLEUR

```asm
# 3e programme ; somme de 2  nombres
.text    
  main:
        # demande d'entrer un nombre
	li $v0, 4               
	la $a0, lecture
	syscall	
	# Lit le nombre et le stocke dans $s0
	li $v0, 5  # le code 5 correspond à la lecture d'une donnée
	syscall
	move $t0, $v0
	# idem pour le 2e nombre
	li $v0, 4               
	la $a0, lecture2
	syscall	
	# Lit le nombre et le stocke dans $s1
	li $v0, 5 
	syscall
	move $t1, $v0
	add $t0, $t0, $t1
	# Affiche le texte d'introduction du résultat
	li $v0, 4
	la $a0, affichage
	syscall
	# Affiche le nombre obtenu
	li $v0, 1
	move $a0, $t0
	syscall
	# termine le programme
	li $v0, 10  # 10 code la terminaison du programme
	syscall      # exécute l'action codée dans $v0 : fin du programme
	
.data    
  lecture: .asciiz "Entrez un nombre : \n"
  lecture2: .asciiz "Entrez un autre nombre : \n"
  affichage: .asciiz "\nLa somme de ces deux nombres est :" 
```
-->


