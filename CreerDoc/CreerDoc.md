# Création d'une documentation Python

## pdoc3

C'est l'outil le plus simple. On l'installe via `pip` ou `conda`

```shell
$ pip install pdoc3
```

La                               documentation                               est
[ICI](https://docs.python.org/fr/3.7/library/pydoc.html#module-pydoc)



Pour générer la documentation au format `html`, il suffit de lancer:

```shell
$ pdoc3 --html Tolkien.py
```

et on obtient [cette page](http://gconnan.free.fr/TEMP/Tolkien_pdoc3.html)

Pour plus d'info, voir [cette page](https://docs.python.org/fr/3.7/library/pydoc.html#module-pydoc)


Pour obtenir un diagramme de classe, on peut utiliser `pyreverse` qui fait
partie de l'extension `pylint`:


```shell
$ pip install pylint
```

Puis on fabrique l'image de la classe:


```shell
$ pyreverse -o jpg Tolkien.py -p Tolkien
```

![pyreverse UML](./classes_Tolkien.jpg)

Pour plus d'info, voir [cette page](https://pdoc3.github.io/pdoc/) et [cette page](https://www.logilab.org/blogentry/6883)


## Doxygen

On  peut de  manière alternative  utiliser `Doxygen`  qui est  un génératier  de
documentation plus généraliste, au départ conçu pour documenter `C` et `C++`.

