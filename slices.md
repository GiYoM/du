Le 19/11/2020 à 09:49, X a écrit :
> J'ai programmé en de nombreux langages dont Pascal, C, java,… Je n'ai jamais vu trace dans ces langages de > slicing et autres particularités des listes python (append, pop, …).

C et Pascal ont 50 ans, un bel âge, certes...pour un être humain. Python a le double de l'âge de nos élèves  : ce n'est plus vraiment un perdreau de l'année.

Jetons un œil sur des langages plus récents...

- KOTLIN : vient d'être adopté comme langage officiel du développement Android. Voici par exemple les [méthodes disponibles pour les listes](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin.collections/-list/)
Il y en a beaucoup ! En particulier `slice`.
Par exemple pour une chaîne :
```kotlin
fun main() {
    val str = "The quick brown fox jumps over the lazy dog"

    println(str.slice(1..14))
    println(str.slice(20 downTo 1))
    println(str.slice(listOf(0,2,4,6,8,10)))
}
```

Donne 
```
he quick brown
j xof nworb kciuq eh
Teqikb
```
- RUST : le langage de Mozilla

```rust
fn main() {
    let vector = vec![1, 2, 3, 4, 5, 6, 7, 8];
    let slice = &vector[3..6];
    println!("length of slice: {}", slice.len()); // 3
    println!("slice: {:?}", slice); // [4, 5, 6]
}
```
 
- GOLANG : le langage de Google
```go
package main
 
import "fmt"
 
func main() {
    var odds =  [8]int{1, 3, 5, 7, 9, 11, 13, 15}
    slice1 := odds[2:]
    fmt.Println(slice1)     // prints [5 7 9 11 13 15]
    slice2 := slice1[2:4]
    fmt.Println(slice2)     // prints [9 11]
}
```

- SCALA : le langage de développement de twitter par exemple
```scala
val phrase = "a soft orange cat"

// Use slice on a string from scala.collection.immutable.StringOps.
// ... Use first index, second index.

val result = phrase.slice(2, 2 + 4)
println(result)
```

Renvoie : `soft`

- SWIFT : le langage de développement d'Apple
```swift

 array = [5, 2, 10, 1, 0, 100, 46, 99]

// From index 3 to index 5
array[3...5] // ArraySlice<Int>[1, 0, 100]

// From index 3 to index less than 5
array[3..<5] // ArraySlice<Int>[1, 0]
```


- ERLANG : le langage de développement de Whatsapp par exemple
```erlang
> lists:sublist([1,2,3,4], 2, 2).
[2,3]
> lists:sublist([1,2,3,4], 2, 5).
[2,3,4]
> lists:sublist([1,2,3,4], 5, 2).
[]
```
et toute la [liste des fonctions disponibles](https://erlang.org/doc/man/lists.html)

- HASKELL : le plus beau...
```haskell
take 2 $ drop 1 $ "Haskell" 
```
renvoie `as`


On peut rajouter des moins sexy :

- C# 
```c#
int[] a = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
var slice = a[i1..i2]; // { 3, 4, 5 }
```

- Java :

```java
Arrays.copyOfRange(myArray, startIndex, endIndex);
```

- PHP :

```php
<?php
$input = array("a", "b", "c", "d", "e");

$output = array_slice($input, 2);      // returns "c", "d", and "e"
$output = array_slice($input, -2, 1);  // returns "d"
$output = array_slice($input, 0, 3);   // returns "a", "b", and "c"
?>
```

- Javascript :
```js
function myFunction() {
  var fruits = ["Banana", "Orange", "Lemon", "Apple", "Mango"];
  var citrus = fruits.slice(1, 3);
}
```
renvoie `Orange Lemon`



...et ce n'est qu'un échantillon