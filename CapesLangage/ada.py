# Coefficient B7 : le programme d'Ada en Python

def ada(n=4):
    # data
    v1, v2, v3 = 1, 2, n

    # working variables
    v4, v5, v6, v7, v8, v9, v10, v11, v12, v13 = 0,0,0,0,0,0,0,0,0,0

    # result variables : B1, B3, B5, B7 (qui devrait être calculé à la fin et valoir -1/30)
    v21, v22, v23, v24  = 1/6, -1/30, 1/42, 0


    # Calculation
    # ------------------------------------------------------------------------
    # ------- A0 -------
    # 01 
    v4 = v5 = v6 = v2 * v3      # 2n
    # 02 
    v4 = v4 - v1                # 2n - 1
    # 03 
    v5 = v5 + v1                # 2n + 1

    # Dans le diagramme d'Ada, on trouve v5 / v4 qui est incorrect.
    # 04 
    v11 = v4 / v5               # (2n - 1) / (2n + 1)

    # 05 
    v11 = v11 / v2              # (1 / 2) * ((2n - 1) / (2n + 1))
    # 06 
    v13 = v13 - v11             # -(1 / 2) * ((2n - 1) / (2n + 1))
    # 07 
    v10 = v3 - v1               # (n - 1), compteur ?

    # A0 = -(1 / 2) * ((2n - 1) / (2n + 1))

    # ------- B1A1 -------
    # 08 
    v7 = v2 + v7                # 2 + 0, instruction MOV
    # 09 
    v11 = v6 / v7               # 2n / 2
    # 10 
    v12 = v21 * v11             # B1 * (2n / 2)

    # A1 = (2n / 2)
    # B1A1 = B1 * (2n / 2)

    # ------- A0 + B1A1 -------
    # 11 
    v13 = v12 + v13            # A0 + B1A1
    # 12 
    v10 = v10 - v1             # (n - 2)

    # 1ere boucle calcule B3A3 et l'ajoute à v13.
    # la 2nde calcule B5A5 et l'ajoute itérativement.
    while v10 > 0:
        # ------- B3A3, B5A5 -------
        while (v6 > 2 * v3 - (2 * (v3 - v10) - 2)):
            # 1ere boucle:
            # 13 
            v6 = v6 - v1           # 2n - 1
            # 14 
            v7 = v1 + v7           # 2 + 1
            # 15 
            v8 = v6 / v7           # (2n - 1) / 3
            # 16 
            v11 = v8 * v11         # (2n / 2) * ((2n - 1) / 3)

            # 2e boucle qui est en fait la même :
            # 17    v6 = v6 - v1              2n - 2
            # 18    v7 = v1 + v7              3 + 1
            # 19    v8 = v6 / v7              (2n - 2) / 4
            # 20    v11 = v8 * v11            (2n / 2) * ((2n - 1) / 3) * ((2n - 2) / 4)

       
        if v10 == 2:
        # 21 
            v12 = v22 * v11            # B3 * A3
        else:
        # 21 
            v12 = v23 * v11            # B5 * A5

        # B3A3 = B3 * (2n / 2) * ((2n - 1) / 3) * ((2n - 2) / 4)

        # ------- A0 + B1A1 + B3A3, A0 + B1A1 + B3A3 + B5A5 -------
        # 22 
        v13 = v12 + v13            # A0 + B1A1 + B3A3 (+ B5A5)
        # 23 
        v10 = v10 - v1             # (n - 3), (n - 4)

    # 24 
    v24 = v13 + v24 # résultat final dans v24
    # 25 
    v3 = v1 + v3    # en avant vers le prochain nb de Bernoulli :)

    ## on obtient bien -1/30 au signe près (Ada explique l'histoire du signe dans ses notes)

    return v24
