# Langages : passé et futur pour s'occuper du présent

[[_TOC_]]



## Robert Floyd et les paradigmes

> In computer science, one sees several such communities, each speaking its own
>language and using its own paradigms. In fact, programming languages typically
>encourage use of some paradigms and discourage others. There are well defined schools
>of Lisp programming, APL programming, Algol programming, and so on. Some regard
>data flow, and some control flow, as the primary structural information about a
>program. Recursion and iteration, copying and sharing of data structures, call by name
>and call by value, all have adherents.


*Robert Floyd - The Paradigms of Programming - Leçon de réception du prix Turing 1979*


## Llull

![./IMG/Llull.jpg](./IMG/Llull.jpg)


## Bouchon, Falcon, Jacquard

![./IMG/carte.jpg](./IMG/carte.jpg)

## Ada

![./IMG/Ada_diagram.jpg](./IMG/Ada_diagram.jpg)

```python
# Coefficient B7 : le programme d'Ada en Python

def ada(n=4):
    # data
    v1, v2, v3 = 1, 2, n

    # working variables
    v4, v5, v6, v7, v8, v9, v10, v11, v12, v13 = 0,0,0,0,0,0,0,0,0,0

    # result variables : B1, B3, B5, B7 (qui devrait être calculé à la fin et valoir -1/30)
    v21, v22, v23, v24  = 1/6, -1/30, 1/42, 0


    # Calculation
    # ------------------------------------------------------------------------
    # ------- A0 -------
    # 01 
    v4 = v5 = v6 = v2 * v3      # 2n
    # 02 
    v4 = v4 - v1                # 2n - 1
    # 03 
    v5 = v5 + v1                # 2n + 1

    # Dans le diagramme d'Ada, on trouve v5 / v4 qui est incorrect.
    # 04 
    v11 = v4 / v5               # (2n - 1) / (2n + 1)

    # 05 
    v11 = v11 / v2              # (1 / 2) * ((2n - 1) / (2n + 1))
    # 06 
    v13 = v13 - v11             # -(1 / 2) * ((2n - 1) / (2n + 1))
    # 07 
    v10 = v3 - v1               # (n - 1), compteur ?

    # A0 = -(1 / 2) * ((2n - 1) / (2n + 1))

    # ------- B1A1 -------
    # 08 
    v7 = v2 + v7                # 2 + 0, instruction MOV
    # 09 
    v11 = v6 / v7               # 2n / 2
    # 10 
    v12 = v21 * v11             # B1 * (2n / 2)

    # A1 = (2n / 2)
    # B1A1 = B1 * (2n / 2)

    # ------- A0 + B1A1 -------
    # 11 
    v13 = v12 + v13            # A0 + B1A1
    # 12 
    v10 = v10 - v1             # (n - 2)

    # 1ere boucle calcule B3A3 et l'ajoute à v13.
    # la 2nde calcule B5A5 et l'ajoute itérativement.
    while v10 > 0:
        # ------- B3A3, B5A5 -------
        while (v6 > 2 * v3 - (2 * (v3 - v10) - 2)):
            # 1ere boucle:
            # 13 
            v6 = v6 - v1           # 2n - 1
            # 14 
            v7 = v1 + v7           # 2 + 1
            # 15 
            v8 = v6 / v7           # (2n - 1) / 3
            # 16 
            v11 = v8 * v11         # (2n / 2) * ((2n - 1) / 3)

            # 2e boucle qui est en fait la même :
            # 17    v6 = v6 - v1              2n - 2
            # 18    v7 = v1 + v7              3 + 1
            # 19    v8 = v6 / v7              (2n - 2) / 4
            # 20    v11 = v8 * v11            (2n / 2) * ((2n - 1) / 3) * ((2n - 2) / 4)

       
        if v10 == 2:
        # 21 
            v12 = v22 * v11            # B3 * A3
        else:
        # 21 
            v12 = v23 * v11            # B5 * A5

        # B3A3 = B3 * (2n / 2) * ((2n - 1) / 3) * ((2n - 2) / 4)

        # ------- A0 + B1A1 + B3A3, A0 + B1A1 + B3A3 + B5A5 -------
        # 22 
        v13 = v12 + v13            # A0 + B1A1 + B3A3 (+ B5A5)
        # 23 
        v10 = v10 - v1             # (n - 3), (n - 4)

    # 24 
    v24 = v13 + v24 # résultat final dans v24
    # 25 
    v3 = v1 + v3    # en avant vers le prochain nb de Bernoulli :)

    ## on obtient bien -1/30 au signe près (Ada explique l'histoire du signe dans ses notes)

    return v24
```



## Model K - 1937

![./IMG/modelK.jpg](./IMG/modelK.jpg)

Additionneur construit sur une table de cuisine : Boole électronique...


## Z2 1937

![./IMG/plan.png](./IMG/plan.png)

Programmable mais pas de branchement (conditionnel)

## Colossus 1944

![./IMG/colossus.jpg](./IMG/colossus.jpg)


## Mark 1 - 1944

![./IMG/mark1.jpg](./IMG/mark1.jpg)

Rouleaux en papier : on les colle pour faire des boucles !

![./IMG/roll.png](./IMG/roll.png)

Une sorte de raccourcis en octal repérant des séquences d'instructions en binaire courantes.



## ENIAC 1946



Le programme n'est pas stocké, seulement les données


![./IMG/eniac.jpg](./IMG/eniac.jpg)




## 1948 - Baby

Le [premier programme](https://web.archive.org/web/20120109142655/http://www.cs.man.ac.uk/CCS/res/res20.htm#e#e)  à tourner sur un  vrai ordi avec une RAM  pour calculer le
plus grand  diviseur de $`2^{18}`$. Il  a fallu 3,5 millions  d'opérations et 52
minutes pour trouver 131 072


```nasm
 1 -18, C // clear accumulator
 2 -19, C // load +a
 3 Sub 20 // Trial subtraction
 4 Test // is difference negative?
 5 Add 21, Cl // still positive, Jump back two lines
 6 Sub 22 // overshot, so add back bn
 7 c, 24 // store +r n
 8 -22, C // load bn
 9 Sub 23 // form b(n+1) = bn - 1
10 c, 20 // store b(n+1)
11 -20, C // load -b(n+1)
12 c, 22 // store it
13 -24, C // load -r n
14 Test // is remainder zero?
15 25, Cl // yes, Jump to line 17
16 23, Cl // no, Jump to line 2 (but see text)
17 Stop // load -b(n+1)

        init     current       final

18        0
19        -a                    -a
20       b1     bn or b(n+1)  b(N+1)
21       -3
22      -b1    -bn or -b(n+1)  -b(N+1)
23       1
24       -       r n            r N
25       16
```

## 1948 - Turing et le Mark 1 de Manchester

INstructions sur 5 bits : les 32 combinaisons sont repérées par un caractère (`/E@A:SIUDRJNFCKTZLWHYPQOBG`).

Un jeune chercheur, Alick Glennie, crée l'*autocode* pour faciliter l'usage du Mark 1, une véritable expérience de langage de programmation, mais il travaillait pour le nucléaire ⇒ non publié.

## 1949 - EDSAC

Sous-programmes "modulaires" : ne dépendent plus des adresses des variables. Jusque là stockés sur des calepins (e.g. racines carrées...)


## 1951 - UNIVAC

![./IMG/univac.jpg](./IMG/univac.jpg)

Premier ordinateur commercial : 46 vendus, 1 million de $ chacun, 14 tonnes...


## Grace Hopper

![./IMG/grace.jpg](./IMG/grace.jpg)

Marre de l'octal (Cela troublait sa comptabilité) !

John Mauchly sort en 1949 le *short code* : `07` pour l'addition etc.

Sur l'UNIVAC, elle crée un outil qui organise les sous-programmes, attribue la mémoire, convertit des ordres donnés. Elle appelle ça un *compilateur*...Elle appela ses compilateurs `A0`, `A1`, etc. mais le premier fut vendu sous le nom de `MATH-MATIC`...
Elle a l'intuition que les ordinateurs sortiront des labos de recherche et des mains des mathématiciens.
Elle s'électionna 30 verbes anglais (avec pour particularité d'avoir la combinaison de la 1èr et la 3e lettre qui différait à chaque fois pour n'utiliser qu'elles). Grace, pour convaincre, crée aussi un langage pour le français et l'allemand. Commercialisation du `FLOW-MATIC`.


## John Backus 

1957 : FORTRAN. Affectation `N = 1`, variables indicées `X(2)`, et surtout la gestion des boucles avec `DO`, amis dépend encore de la machine sur laquel il tourne (IBM 704 au début).

## Années 60 : COBOL

Common Business Oriented Language : il doit marcher partout...et il marche encore...
`SI SALAIRE SUPÉRIEUR À 2000, ALORS EXÉCUTER ROUTINE-VÉRIFICATION-SALAIRE`
Dénigré par l'aristocratie mathématique. Servit à Grace Hopper à communiquer avec des japonais : `MOVE`, `GOTO`....

Mais à cette époque, une guerre eut lieu entre Européens et Américains alors que depuis plusieurs années ils essaient de mettre au point en commun LE langage : ALGOL. Leur litige ? le point/la virgule...Backus intervient et jette les bases de la `Backus Normal Form`.

Mais des dizaines de variantes naissent suite à ces désaccords:

![./IMG/babel.jpeg](./IMG/babel.jpeg)


## BASIC et Robert Albrecht : naissance de la didactique informatique

BASIC :  *Beginner's All-Purpose  Symbolic Instruction  Code* introduit  en 1964
pour devenir programmeur en quelques heures.  La révolution :les étudiants
pouvaient, via  un terminal,  accéder à l'ordinateur  du Dartmouth  College sans
carte perforée,  sans connaissances en  informatique, en tâtonnant. Ça  vous dit
quelque  chose  ?  L'informatique  quittait  le cercle  des  initiés.  Deux  ans
auparavant [Robert Albrecht](http://iae-pedia.org/Robert_Albrecht)  avait mis au
point  BEFORTRAN. Avec  le BASIC,  il  découvre un  moyen de  faire partager  la
programmation au plus grand nombre. Il  fonda la SHAFT (*Society to Help Abolish
FORTRAN Teaching*). 

Sur  [cette   page](https://www.zx81stuff.org.uk/zx81/jtyone.html)  vous  pouvez
jouer avec un ZX81 (qui comme son nom l'indique est arrivé en 1981) et constitua
aussi une révolution...mon premier ordinateur...

```realbasic
10 REM MON PROGRAMME DE RACINE CARREE
20 PRINT "ENTREZ UN NOMBRE POSITIF"
30 INPUT A
40 IF A<0 THEN GOTO 20
50 PRINT "A = ";A;" RACINE(A) = ";SQR A
60 GOTO 20
```

Puis en 1966,  Albrecht rejoignit en Californie  une communauté d'informaticiens
marginaux désireux de mettre la puissance de l'informatique à la portée de tous.
Il lança  dans les  années 1970  un parc  de loisirs  informatique à  Menlo Park
(là  où Google  a été  créé dans  un garage  et siège  de FB...)  au sein  de la
[PCC](https://en.wikipedia.org/wiki/People%27s_Computer_Company): 
"*"Computers  are mostly  used against  people instead  of for  people; used  to
control  people instead  of to  free them;  Time to  change all  that -  we need
a... Peoples Computer Company."*"
Ils       publièrent        de       nombreux        magazines       disponibles
[ici](https://www.computerhistory.org/collections/catalog/102661095)

En 1975, deux professeurs dont un  du secondaire bidouillèrent un Altair 8800 et
mirent au point un compilateur Tiny BASIC qui fut distribué gartuitement sous forme
d'instructions en  octal. Mais certains surfèrent  sur le succès du  BASIC et le
firent basculer du côté monnayable de la force dont un certain Bill Gates.

Mais le  BASIC des  années 1960  n'était plus vraiment  adapté aux  machines des
années 1980  et on préféra le  PASCAL qui lui  aussi se perdit dans  de nombreux
dialectes. Son  concepteur corrigea  le tir  avec MODULA-2  mais entre  temps le
génie marketing et  informatique d'un aventurier hors norme contra  le projet le
TURBO PASCAL de Philippe Kahn.


## Pendant ce temps, dans les labos du MIT, de Luminy,...

John Mc Carthy mettait au point LISP pour ses recherches en IA. En LISP, tout est liste. Un langage qui ne servait pas simplement à faire des choses mais qui était beau...


À Marseille,  on met  au point  le PORLOG: on  n'explique plus  à la  machine ce
qu'elle doit  faire, on  lui fournit des  faits et des  relations entre  eux. La
machine en tire des déductions.

Un autre OVNI  est l'APL : basé sur  un alphabet de 95 signes.  Très concis mais
même les concepteurs d'un programme ne le comprennent pas...


## C

INtimement lié la programmation  d'UNIX et à ses deux héros  : Dennis Ritchie et
Ken Thompson. Mais vous connaissez tous.

## Un peu de structure : ALGOL, PASCAL, ADA

1972 : Hoare,  Dahl et Dijkstra publient *la programmation  structurée*. Fini le
bricolage,  les ingénieurs  arrivent et  avec  eux le  PASCAL. C'est  la fin  du
détesté `GOTO`.

Les machines  coûtent moins cher  mais les logiciels  grèvent le budget  car ils
sont impossibles à  maintenir. Surtout dans l'armement...Cela  donnera ADA. Mais
trop compliqué.


## use the FORTH

Un langage concis où tout est basé sur la structure de pile [tutoriel](http://www.exemark.com/FORTH/StartingFORTHfromForthWebsitev9_2013_12_24.pdf). 

```forth
 : FLOOR5 ( n -- n' )   DUP 6 < IF DROP 5 ELSE 1 - THEN ;
```

équivalent à 

```python
def floor5(n: int) -> int:
    if n < 6:
	    return 5
	else:
		return n - 1
```

ou 

```python
def floor5(n: int) -> int:
    return 5 if n < 6 else n - 1
```


Pédagogiquement           parlant,           on           peut           essayer
[joy](https://www.latrobe.edu.au/humanities/research/research-projects/past-projects/joy-programming-language)
ou [cat](https://github.com/cdiggins/cat-language)
qui ne fonctionne qu'avec une pile et sans variable

Voici par exemple la factorielle...trop drôle :

```cat
define fact : (int -> int)
{{
  desc:
    La factorielle féline
  tests:
    in: 5 fact
    out: 120
}}
{
  eqz
  [pop 1]
  [dup dec fact mul_int]
  if
}
```


## Les années 1990

Dans  des recoins  obscurs  naissent  de nouveaux  langages  : Python,  Haskell,
Caml...


## 1995 : l'année où tout a basculé...vers la POO

[![](https://img.youtube.com/vi/pHxtB8zr8UM/0.jpg)](https://www.youtube.com/watch?v=pHxtB8zr8UM "Java Ad")





## SCALA

Tri rapide:

```scala
def sort(xs: Array[Int]): Array[Int] = {
    if (xs.length <= 1) xs
    else {
        val pivot = xs(xs.length / 2)
        Array.concat(sort(xs filter (pivot >)),
                     xs filter (pivot ==),
                     sort(xs filter (pivot <)))
```





## Rust

Le premier  noyau stable  a été  publié en  2015. C'est  un langage  soutenu par
Mozilla. Son nom traduit le fait que Rust s'appuie sur des concepts qui ont fait
leurs preuves mais qui ne sont pas forcément réunis dans un même langage. Rust a
de nombreux concepts tirés du monde fonctionnel:

* typage statique
* variables immuables
* inférence de type
* filtrage par motif
* généricité

Il introduit un concept original d'appartenance et d'emprunt.

```rust
fn main() {
   let x = 1;
   x = 2; 
   let v = vec![1, 2, 3];
   let w = v;
   println!("{}", v[0]);
}
```

On lance la compilation et voici le résultat :

```rust

error[E0384]: cannot assign twice to immutable variable `x`
 --> ./UCO/DU/Bloc5/rust.rs:3:4
  |
2 |    let x = 1;
  |        -
  |        |
  |        first assignment to `x`
  |        help: make this binding mutable: `mut x`
3 |    x = 2;    
  |    ^^^^^ cannot assign twice to immutable variable

error[E0382]: borrow of moved value: `v`
 --> ./UCO/DU/Bloc5/rust.rs:6:19
  |
4 |    let v = vec![1, 2, 3];
  |        - move occurs because `v` has type `std::vec::Vec<i32>`, which does not implement the `Copy` trait
5 |    let w = v;
  |            - value moved here
6 |    println!("{}", v[0]);
  |                   ^ value borrowed here after move

error: aborting due to 2 previous errors

Some errors have detailed explanations: E0382, E0384.
For more information about an error, try `rustc --explain E0382`.
```



## Crystal


Crystal est un langage encore plus récent, inspiré de Ruby, où tout est objet mais
où le  typage est  statique et  permet une  inférence de  type. La  syntaxe veut
éviter le  *boilerplate code* et  apparaît comme  plus sympathique que  celle de
Python. 

```crystal
class Animal
end

class Dog < Animal
  def talk
    "Woof!"
  end
end

class Cat < Animal
  def talk
    "Miau"
  end
end

class Person
  getter pet

  def initialize(@name : String, @pet : Animal)
  end
end

john = Person.new "John", Dog.new
peter = Person.new "Peter", Cat.new


john.pet.talk #=> "Woof!"
```


## Elixir

Regardons la doc

*Elixir is a dynamic, functional language designed for building scalable and maintainable applications.
Elixir leverages the  Erlang VM, known for running  low-latency, distributed and
fault-tolerant systems,  while also being  successfully used in  web development
and the embedded software domain.*


```elixir
current_process = self()

# Spawn an Elixir process (not an operating system one!)
spawn_link(fn ->
  send(current_process, {:msg, "hello world"})
end)

# Block until the message is received
receive do
  {:msg, contents} -> IO.puts(contents)
end
```


Fault tolerance

```elixir
children = [
  TCP.Pool,
  {TCP.Acceptor, port: 4040}
]

Supervisor.start_link(children, strategy: :one_for_one)
```

Functionnal programming:

```elixir
def drive(%User{age: age}) when age >= 16 do
  # Code that drives a car
end

drive(User.get("John Doe"))
#=> Fails if the user is under 16
```




## Kotlin

 Kotlin est devenu  officiellement le 9 mai dernier le  langage de programmation
 voulu et  recommandé par le  géant américain  Google pour le  développement des
 applications Android !
 
 
Inspiré de Ruby qui lui donne son aspect objet, il est aussi très fonctionnel:

```kotlin
// data class with parameters and their optional default values
data class Book(val name: String = "", val price: Int = 0)

fun main(args: Array) {
    // create a data class object like any other class object
    var book1 = Book("Kotlin Programming",250)
    println(book1)
    // output: Book(name=Kotlin Programming, price=250)
}
```
 
 
 ```kotlin
 // the following function takes a lambda, f, and executes f passing it the string, "lambda"
// note that (s: String) -> Unit indicates a lambda with a String parameter and Unit return type
fun executeLambda(f: (s: String) -> Unit) {
    f("lambda")
}
 ```
 
 
```kotlin
// the following statement defines a lambda that takes a single parameter and passes it to the println function
val l = { c : Any? -> println(c) }
// lambdas with no parameters may simply be defined using { }
val l2 = { print("no parameters") }
```



## Julia

```julia
julia> println(filter(i -> (2i^2 - 10 >= 5), [1, 2, 3, 4, 5]))
[3, 4, 5]
```





## Go

Développé  pour  Google  par  des  stars (Robert  Griesemer,  Rob  Pike  et  Ken
Thompson), c'est un langage au typage fort, statique et structurel.

D'après Wikipedia:
*S'il  vise aussi  la  rapidité d'exécution,  indispensable  à la  programmation
système, il considère le multithreading comme le moyen le plus robuste d'assurer
sur  les processeurs  actuels cette  rapidité8  tout en  rendant la  maintenance
facile par séparation  de tâches simples exécutées  indépendamment afin d'éviter
de  créer  des  «  usines  à   gaz  ».  Cette  conception  permet  également  le
fonctionnement sans  réécriture sur des architectures  multi-cœurs en exploitant
immédiatement l'augmentation de puissance correspondante.*


```go
func main() {
	var list = []string{"Orange", "Apple", "Banana", "Grape"}
	// we are passing the array and a function as arguments to mapForEach method.
	var out = mapForEach(list, func(it string) int {
		return len(it)
	})
	fmt.Println(out) // [6, 5, 6, 5]

}

// The higher-order-function takes an array and a function as arguments
func mapForEach(arr []string, fn func(it string) int) []int {
	var newArray = []int{}
	for _, it := range arr {
		// We are executing the method passed
		newArray = append(newArray, fn(it))
	}
	return newArray
}
```

## Elm


Du pur fonctionnel qui s'exporte en javascript : "*It's been weird transitioning
to a language  /framework that Ιcan just  trust. I don't really need  to move to
the browser and see if everything secretly broke*".


```elm
-- Commentaire sur une ligne

{- Ceci est un commentaire multiligne.
   Il peut continuer sur plusieurs lignes.
-}

{- Il est possible {- d'imbriquer -} les commentaires multilignes -}

-- Définition d'une valeur nommée « greeting ». Le type est inféré comme un « String ».

greeting =
    "Hello World!"

-- Il est préférable d'ajouter l'annotation des types pour les déclarations de haut-niveau.

hello : String
hello =
    "Hi there."

-- Les fonctions sont déclarées de la même façon, avec les arguments qui suivent le nom de la fonction.

add x y =
    x + y

-- Il est préférable d'annoter les types pour les fonctions aussi.

hypotenuse : Float -> Float -> Float
hypotenuse a b =
    sqrt (a^2 + b^2)

-- Le « if » est une expression : il retourne une valeur.

absoluteValue : Int -> Int
absoluteValue number =
    if number < 0 then -number else number

-- Les enregistrements sont utilisés pour grouper les valeurs avec des noms.

book : { title:String, author:String, pages:Int }
book =
    { title = "Steppenwolf"
    , author = "Hesse"
    , pages = 237 
    }

-- Il est possible de créer un nouveau type avec le mot-clé « type ».
-- Ce qui suit représente un arbre binaire.

type Tree a
    = Empty
    | Node a (Tree a) (Tree a)

-- Il est possible d'inspecter ces types avec une expression « case ».

depth : Tree a -> Int
depth tree =
    case tree of
      Empty -> 0
      Node value left right ->
          1 + max (depth left) (depth right)
```
