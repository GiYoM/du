# Pointeurs, valeurs, références : Python vs C

Sans parler de ce que nous devons dire aux élèves, il est utile de savoir ce qui
se passe avec  le *passage de valeurs*  d'une fonction. On parle  de passage par
valeurs, par référence, par objet, par nom, par nécessité, par futur...

Qu'en est-il en général?  En Pyhton? En Cpython? Est-ce du  ressort du langage ?
De ses implémentations ?

<!--
## In, Out and InOut 

Beaucoup de noms traînent  autour de cette notion de "passage"  mais en fait, il
n'y a que trois *modes*: le In, le Out, le InOut...

![inout](./IMG/InOut.jpg)



*  dans   le  mode  IN   de  transmission,   les  données  sont   transmises  au
  "sous-programme". Cela se  passe en **lecture seule**.  Le sous-programme peut
  lire les données mais ne peut pas les modifier. C'est comme une constante dans
  la limite de la portée (scope) du sous-programme.
* dans le mode OUT, les données sont reçues dans le programme appelant depuis le sous-programme. 
* dans le mode INOUT, il y a les deux transmissions.
-->

## En Python, pas de variables, des noms


### En C, des variables

En C, il y a des variables et des affectations. Quand on écrit:

```c
int a = 42;
```

1. on alloue un espace mémoire correspondant à un `int`
2. on met 42 dans cet espace mémoire
3. on indique que `a` est "l'étiquette" de cette case mémoire

Si on change  la valeur étiquetée par `a`,  on change juste le contenu  de la case
mémoire. C'est classiquement une *variable* car elle *peut varier*.

Par exemple avec ce code:

```c
#include <stdio.h>

int main(){
  int a = 42;
  
  printf("a = %i\n", a);
  printf("l'adresse de a est %p\n", (void *)&a);

  a = a + 42;

  printf("a = %i\n", a);
  printf("l'adresse de a est %p\n", (void *)&a);

  return 0;
}
```

on obtient:

```console
$ gcc varC.c -o varC.out -std=c99 -Wall -Wextra -pedantic && ./varC.out &

a = 42
l'adresse de a est 0x7ffcddafe064
a = 84
l'adresse de a est 0x7ffcddafe064
```


La valeur  a changé  mais pas l'adresse.  Si on crée  une nouvelle  variable `b`
affectée à `a` et un pointeur `p` qui est affecté à l'adresse de `a`:

```c
  int b = a;

  printf("b = %i\n", b);
  printf("l'adresse de b est %p\n", (void *)&b);
  
  int *p;
  p = &a;

  printf("Le pointeur p = %p\n", (void *)p);
  printf("est-ce que a == *p ? Réponse : %i (qui veut dire OUI)\n", a==*p);
  printf("en effet, *p = %i\n", *p);

```

On obtient:

```console
a = 42                                                            
l'adresse de a est 0x7fff04fe1958
a = 84
l'adresse de a est 0x7fff04fe1958
b = 84
l'adresse de b est 0x7fff04fe195c
Le pointeur p = 0x7fff04fe1958
est-ce que a == *p ? Réponse : 1 (qui veut dire OUI)
en effet, *p = 84
```

Avec `a` et `b`, on a deux espaces mémoire différents mais qui contiennent en leur sein la même
valeur. `p` est l'adresse de `a` et `*p` est donc `a`


### En Python, des noms


Quand on écrit:

```python
>>> a = 4200
```

il  se  passe beaucoup  de  choses  mais la  principale  est  que cela  crée  un
[`PyObject`](https://github.com/python/cpython/blob/master/Include/object.h)
dans  `CPython`, l'implémentation  en  C  de Python.  Les  `PyObjects` sont  des
`struct` C où sont définis un *Reference Count* pour savoir si le `PyObject` est
encore lié à un objet Python, des pointeurs vers le type et la valeur de l'objet
(et aussi d'autres choses...).

Maintenant `a`  ne "possède"  pas la  valeur du  `PyObject` (et  on ne  peut pas
avoir accès au `PyObject`...normalement). 

Si on écrit :

```python
>>> a = 4242
```

alors `a` est lié à un autre  `PyObject`, l'ancien ayant son `ref count` diminué
de 1 et s'il est à 0, il sera supprimé par le ramasse-miettes.


Voilà qui explique que si on écrit 

```python
>>> b = a
```

alors `b` sera lié au même `PyObject`:


```python
In [167]: a = 4200

In [168]: id(a)
Out[168]: 139925149353232

In [169]: a = a + 1

In [170]: id(a)
Out[170]: 139925149353808

In [171]: b = a

In [172]: id(b)
Out[172]: 139925149353808

In [173]: b is a
Out[173]: True

In [174]: b  = b + 42

In [175]: b is a
Out[175]: False
```


#### La blague

Bon, on a compris :

```python
In [195]: a = 4200

In [196]: b = 2100 + 2100

In [197]: id(a)
Out[197]: 139925142179920

In [198]: id(b)
Out[198]: 139925142181264

In [199]: a is b
Out[199]: False
```

Maintenant, faisons la même chose via une fonction:


```python
def idai():
    a = 4200
    b = 2100 + 2100
    print(id(a))
    print(id(b))
    print(a is b)
```

et appelons-la:

```python
In [200]: idai()
139925149354736
139925149354736
True
```

fichtre...

En  fait, cette  fois le  compilateur  a eu  la possibilité  d'aller
optimiser les choses :  il a vu que `a` et `b` étaient  liés au même entier donc
autant n'utiliser qu'un seul `PyObject` pour ça.


Ça y est, on comprend...mais tout se trouble:

```python
In [206]: a = 42

In [207]: b = 21 + 21

In [208]: id(a)
Out[208]: 94041155143744

In [209]: id(b)
Out[209]: 94041155143744

In [210]: a is b
Out[210]: True
```

patatras...mais non en fait  ! Il y a encore une optimisation  cachée : pour les
entiers entre -5 et 256 ainsi que les chaînes de moins de 20 caractères ne contenant que des lettres
ASCII, des chiffres ou un tiret bas, ceux-ci se trouvent dans le dictionnaire `globals()` par défaut et cela
évite de créer des `PyObjects` inutilement.  En effet, ces valeurs se retrouvent
très souvent dans un usage quotidien.

```python
In [223]: c = "bulgroz"

In [224]: id(c)
Out[224]: 139925141251632

In [225]: d = "bulgroz"

In [226]: id(d)
Out[226]: 139925141251632

In [227]: c = "eviv bulgroz"

In [228]: id(c)
Out[228]: 139925141250928

In [229]: d = "eviv bulgroz"

In [230]: id(d)
Out[230]: 139925141253744

In [231]: c = "eviv_bulgroz"

In [232]: id(c)
Out[232]: 139925148750128

In [233]: d = "eviv_bulgroz"

In [234]: id(d)
Out[234]: 139925148750128

In [235]: c = "bùlgroz"

In [236]: id(c)
Out[236]: 139925171561840

In [237]: d = "bùlgroz"

In [238]: id(d)
Out[238]: 139925171562704
```

Donc par exemple, on privilégiera les  chaînes sans accents ni espaces comme clé
de dictionnaires.


### Mutables/non mutables

On a  observé certaines choses  car on a travaillé  sur des entiers,  objets non
mutables. C'est différent si on travaille sur des objets de type `list`, `set` ou
`dict` ou autres types faits maison.

```python
In [263]: x = 1

In [264]: id(x)
Out[264]: 94041155142432

In [265]: x += 1

In [266]: id(x)
Out[266]: 94041155142464

In [267]: xs = [1]

In [268]: id(xs)
Out[268]: 139925155692784

In [269]: xs += [1]

In [270]: id(xs)
Out[270]: 139925155692784

In [271]: es = {1}

In [272]: id(es)
Out[272]: 139925150115552

In [273]: es |= {2}

In [274]: id(es)
Out[274]: 139925150115552
```




## Passage par valeur/référence/autre chose


### Avec C

C ne fait que des passages par valeurs: 

```c

void plus_un(int x){
  x += 1;
  printf("l'adresse de la variable modifiée est %p\n", (void *) &x);
}

void plus_un_p(int *x) {
    *x += 1;
    printf("l'adresse de la variable modifiée est %p\n", (void *) x);
}


int main(){
  int a = 84;
  
  printf("a = %i\n", a);
  printf("l'adresse de a est %p\n", (void *)&a);

  printf("après appel à plus_un: \n");
  plus_un(a);
  printf("a = %i\n", a);
  printf("l'adresse de a est %p\n", (void *)&a);
  printf("a n'est pas modifié : passage par valeur\n");
  
  
  printf("après appel à plus_un_p : \n");
  plus_un_p(&a);
  printf("a = %i\n", a);
  printf("l'adresse de a est %p\n", (void *)&a);
  printf("a est modifié : on a simulé un passage par référence en passant par valeur le pointeur\n");
 
  return 0;
}
```

Et on obtient bien:

```console
a = 84
l'adresse de a est 0x7ffd53ccc218
après appel à plus_un: 
l'adresse de la variable modifiée est 0x7ffd53ccc1fc
a = 84
l'adresse de a est 0x7ffd53ccc218
a n'est pas modifié : passage par valeur
après appel à plus_un_p : 
l'adresse de la variable modifiée est 0x7ffd53ccc218
a = 85
l'adresse de a est 0x7ffd53ccc218
a est modifié : on a simulé un passage par référence en passant par valeur le pointeur
```

En passage par valeur,  on ne modifie pas la variable : on  en crée une nouvelle
qui reste limitée à  la portée de la fonction. On a  avec `plus_un_p` un nouveau
pointeur, qui a une nouvelle adresse, certes,  mais on s'en fiche car il a gardé
l'adresse de `a` comme valeur et a donc  permis de modifier la valeur de `a`. On
a travaillé sur 3 niveaux (adresse  du pointeur, adresse pointée par le pointeur
(donc sa valeur) et valeur située  à l'emplacement mémoire désigné par la valeur
du pointeur...ouch)

### Avec Python

Reprenons le `refcount` qui est accessible par `sys.getrefcount()` de la
bibliothèque  `sys` pour  voir  comment  agit Python  avec  les arguments  d'une
fonction  (on parlera  franglais  :  argument veut  dire  paramètre effectif  et
paramètre signifiera paramètre formel).

Créons une fonction:

```python
def f(t: list):
    print(sys.getrefcount(t))
	t.append(1)
```

et observons:

```python
In [21]: truc = [1, 2]

In [22]: sys.getrefcount(truc)
Out[22]: 2

In [23]: f(truc)
4

In [24]: truc
[1, 2, 1]
```

Le `refcount` a  été incrémenté de 2 : lors  de l'appel à `f`, un nom  `t` a été
créé et lié au même `PyObject` déjà lié à `truc` et qui contient `[1, 2]`
et ça donne ça via pythontutor:


![pyt](./IMG/pt.png)

On voit que deux noms sont liés à `[1, 2]`. Lors de l'appel de `f`, une copie de
`truc` est liée au nom `t`. L'argument est donc d'abord évalué puis passé à la fonction.

Ça ressemble donc à un passage  par valeur...mais aussi un passage par référence
car `truc` a été modifiée...

Cela est  possible car la  liste est mutable. Avec  un argument non  mutable, on
n'aurait pas cet aspect de passage par référence.

On parle  plutôt dans le monde  Python de passage  par assignation : comme  vu à
travers le `refcount`,  les noms des paramètres sont liés  aux arguments en tant
qu'objets à l'appel de la fonction.


Voyez ce qui se passe ici :

```python
def reff(xs):
    print(f'xs = {xs} et a pour id {id(xs)}')
    xs += '1'
    print(f'xs = {xs} et a pour id {id(xs)}')
```

Puis

```python
In [120]: xs = [1, 2]

In [121]: reff(xs)
xs = [1, 2] et a pour id 140176696745728
xs = [1, 2, '1'] et a pour id 140176696745728

In [122]: print(f'xs = {xs} et a pour id {id(xs)}')
xs = [1, 2, '1'] et a pour id 140176696745728

In [123]: xs = '000'

In [124]: reff(xs)
xs = 000 et a pour id 140176696715440
xs = 0001 et a pour id 140176696264624

In [125]: print(f'xs = {xs} et a pour id {id(xs)}')
xs = 000 et a pour id 140176696715440
```

Mutable/non mutable :l'appel de la même fonction ne va pas avoir la même action.



Pour simuler des passages par référence pour des objets non mutables, il faudrait des pointeurs mais
Python n'a pas de pointeurs...


### Passage par nécessité


Le plus beau pour la fin. En Haskell:

```haskell
Prelude> f (x,y) = x
Prelude> f (1, 1/0)
1
```

alors qu'en Python:

```python
In [1]: f = lambda x,y: x                                                                     

In [2]: f (1, 2)                                                                            
Out[2]: 1

In [3]: f (1, 1/0)                                                                          
---------------------------------------------------------------------------
ZeroDivisionError                         Traceback (most recent call last)
<ipython-input-4-225685af8d37> in <module>
----> 1 f (1, 1/0)

ZeroDivisionError: division by zero
```


