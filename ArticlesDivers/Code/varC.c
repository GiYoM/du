#include <stdio.h>

void plus_un(int x){
  x += 1;
  printf("l'adresse de la variable modifiée est %p\n", (void *) &x);
}

void plus_un_p(int *x) {
    *x += 1;
    printf("l'adresse de la variable modifiée est %p\n", (void *) x);
}


int main(){
  int a = 42;
  
  printf("a = %i\n", a);
  printf("l'adresse de a est %p\n", (void *)&a);

  a = a + 42;

  printf("a = %i\n", a);
  printf("l'adresse de a est %p\n", (void *)&a);

  int b = a;

  printf("b = %i\n", b);
  printf("l'adresse de b est %p\n", (void *)&b);

  int *p;
  p = &a;

  printf("Le pointeur p = %p\n", (void *)p);
  printf("est-ce que a == *p ? -> %i\n", a==*p);
  printf("en effet, *p = %i\n", *p);

  printf("a = %i\n", a);
  printf("l'adresse de a est %p\n", (void *)&a);


  
  printf("après appel à plus_un: \n");
  plus_un(a);
  printf("a = %i\n", a);
  printf("l'adresse de a est %p\n", (void *)&a);
  printf("a n'est pas modifié : passage par valeur\n");
  
  
  
  printf("après appel à plus_un_p : \n");
  plus_un_p(&a);
  printf("a = %i\n", a);
  printf("l'adresse de a est %p\n", (void *)&a);
  printf("a est modifié : on a simulé un passage par référence en passant par valeur le pointeur\n");

  
  return 0;
}
