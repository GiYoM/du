def f(x, y):
    z = 'machin'
    loc = locals()
    print(loc)
    print(locals())
    w = 'truc'
    print(loc)
    print(locals())
    
x = 1
    
def g():
    x = 'bidule'
    print(x)
    print(locals())

xs = ['un', 'deux', 'trois']

def h():
    xs = ['un']
    print(f"avant changement le xs local vaut {locals()['xs']}")
    print(f"avant changement le xs global vaut {globals()['xs']}")
    xs[0] = 'rast'
    print(f"après changement le xs local vaut {locals()['xs']}")
    print(f"après changement le xs global vaut {globals()['xs']}")

    
def h2():
    global xs
    print(f"avant changement le xs global vaut {globals()['xs']}")
    xs[0] = 'rast'
    print(f"après changement le xs global vaut {globals()['xs']}")


    
def h3(xs):
    print(f"avant changement le xs local vaut {locals()['xs']}")
    print(f"avant changement le xs global vaut {globals()['xs']}")
    xs[0] = 'rast'
    print(f"après changement le xs local vaut {locals()['xs']}")
    print(f"après changement le xs global vaut {globals()['xs']}")


def h5():
    #print(locals()['xs'])
    print(globals()['xs'])
    xs[0] = 'adine'
    #print(locals()['xs'])
    print(globals()['xs'])

xs = [1, 2, 3]

def i(xs):
    print(locals()['xs'])
    print(globals()['xs'])
    xs[0] = 'rast'
    print(locals()['xs'])
    print(globals()['xs'])

def i2(xs = [4, 5, 6]):
    print(locals()['xs'])
    print(globals()['xs'])
    xs[0] = 'rast'
    print(locals()['xs'])
    print(globals()['xs'])

    



def idai():
    a = 4200
    b = 2100 + 2100
    print(id(a))
    print(id(b))
    print(a is b)


import sys
    
def f(t: list):
    print(sys.getrefcount(t))
    print(t)
    t.append(1)
    print(sys.getrefcount(t))
    print(t)

    #return t[0]

def reff(xs):
    print(f'xs = {xs} et a pour id {id(xs)}')
    xs += '1'
    print(f'xs = {xs} et a pour id {id(xs)}')

