

## Adresses IP et la norme CIDR (Fiche 28)

À l'intérieur d'un **même réseau**, toute machine reçoit une adresse IP unique. Il existe
deux formats: un  de longueur 4 octets  (IPv4) et un autre de  longueur 6 octets
(IPv6). 

1. Une adresse IPv4 étant codée sur  4 octets, expliquez pourquoi un codage sur
   6 octets a été introduits il y a quelques années.

2. Les adresses IPv4  sont souvent notées en décimal, la  valeur de chaque octet
   étant  séparée   par  un  point.   Voici  par   exemple  une  adresse   IP  :
   `188.72.99.81`.
   Écrivez une  fonction Python qui  écrit une  adresse IPv4 en binaire. On
   pourra utiliser la méthode `split` et saon homologue `join`.
   
   On  pourra commencer  par fabriquer  une  fonction qui  transforme un  entier
   inférieur à 255 en une chaîne de 8 bits.

3. En fait l'adresse IPv4 contient  deux parties: une partie identifie le réseau
   auquel  appartient la  machine et  la seconde  identifie la  machine dans  le
   réseau.  Deux machines  peuvent  ainsi  savoir si  elles  sont  dans le  même
   réseau.  L'adresse du  réseau  commun  est connu  grâce  au CIDR  (*Classless
   Inter-Domain Routing*). 
   Par exemple, si j'ai  besoin dans mon école de 100 adresses,  on peut me donner
   l'adresse `206.65.12.0/25`  : cela signifie  que les  25 bits de  poids forts
   seront les mêmes  pour toutes les machines  du réseau, et cela  nous laisse 7
   bits pour distinguer les machines dans ce réseau.
   a. Est-ce assez pour donner une adresse au 100 machines ?
   b. Imaginez un moyen  de vérifier que deux machines sont  dans le même réseau
      si l'on connaît le CIDR. Codez-le en Python.
	  



<!--
  ## Correction Adresse Ip
  
1. Sur 32 bits  on peut coder au maximum $2^{32}$ adresses soit  de l'ordre de 4
   milliards th\'eoriquement.  En fait, c'est  encore moins en  pratique. Toutes
   les adresses  étaient utilisées en 2011.  Mais un effort pour  optimiser leur
   allocation a  été fait et  permet de continuer  à utiliser les  adresses IPv4
   malgré  la  prolifération  des  appareils  connectés.  On  estime  le  nombre
   d'appareils connectés dans le monde à  27 milliards en 2019, ce nombre devant
   passer à 75 milliards en 2025 !
  
2. On peut  utiliser `split` qui explose  une chaîne et `join`  qui au contraire
   la   reconstruit.  On   entre  l'adresse   IP   sous  forme   de  chaîne   de
   caractères. `hex(int(car))` transforme un entier  en base 10 entré sous forme
   de chaîne de caractères en une chaîne représentant son écriture en base 16.
   
```python
def ip2hex(ip):
    octets = [hex(int(x))[2:] for x in ip.split('.')]
    return ''.join(octets)
```
	On peut g\'en\'eraliser et mettre le format du nombre en argument:
	
```python
def ip2format(ip,format):
    octets = [format(int(x))[2:] for x in ip.split('.')]
    return ''.join(octets)
```

et on obtient:

```python
>>> ip2format('12.23.33.48',hex)
'c172130'

>>> ip2format('12.23.33.48',bin)
'110010111100001110000'
```

3.  a.  On dispose  de  7  bits donc  128  possibilités  d'adresses. C'est  donc
      suffisant.
	  
  b. On peut extraire les 25 premiers bits de chaque adresse et les comparer. 
  
  On peut par exemple utiliser nos fonctions de la question précédente :
  
```python
def dec2octet(d):
    """
    On entre un nombre inférieur à 255 en base 10 sous forme de chaîne
    On obtient son écriture en base 2 sur un octet
    """
    assert int(d) < 255, " Le nombre %d ne peut pas être écrit sur un octet"%d
    b = bin(int(d))[2:]
    return ('0'*(8-len(b))) + b
    
def ip2bin(ip):
    """
    Renvoie la représentation binaire d'une adresse IP sur 32 bits
    """
    octets = [dec2octet(x) for x in ip.split('.')]
    return ''.join(octets)

def meme_reseau(ip1, ip2, cidr):
    """
    Teste si 2 adresses IP correspondent au même réseau
    en travaillant sur les chaînes
    """
    ipb1 = ip2bin(ip1)
    ipb2 = ip2bin(ip2)
    return ipb1[:cidr] == ipb2[:cidr]

def masque_reseau(ip1, ip2, cidr):
    """
    Teste si 2 adresses IP correspondent au même réseau
    en travaillant sur les entiers et les opérations bit
    à bit
    """
    ipb1 = eval('0b' + ip2bin(ip1)) >> (32 - cidr)
    ipb2 = eval('0b' + ip2bin(ip2)) >> (32 - cidr)
    return ipb1 == ipb2  
```
  
Par exemple:

```python
>>> masque_reseau('206.65.12.67', '206.65.12.129',25)
False

>>> masque_reseau('206.65.12.67', '206.65.12.127',25)
True
```
  
-->

