import time

def crible(n):
    barre = [False]*n
    prems = [2]
    k = 3
    while k < n:
        if not barre[k]:
            prems.append(k)
            for m in range(k*2, n, k):
                barre[m] = True
        k += 2
    return prems

a = time.time()
crible(1000000)
b = time.time()


a2 = time.time()
crible(4000000)
b2 = time.time()
print((b2-a2)/(b-a))


