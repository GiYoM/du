# Réseaux

## Généralités

Dès que deux  entités ou plus veulent  communiquer, un réseau se  crée. Ce n'est
donc pas un domaine né avec l'informatique.


### Circuits/paquets

Les communications téléphoniques du XXe siècle, quand les téléphones avaient des
fils, est un  cas typique de r'eseau  organisé en circuits :  pour téléphoner de
Nantes à Montaigu, il faut un circuit totalement dédié à cette communication, et
les    câbles    et    modulateurs    utilisés    seront    dédiés    à    cette
communication.   L'avantage  est   qu'on  est   à  peu   près  sûr   de  pouvoir
communiquer. Le désavantage est que cela coûte cher.

La communication  peut sinon  être basée  sur des paquets  : chaque  message est
divisé en  paquets qui  se promène à  travers le réseau  en suivant  des chemins
éventuellement différents  et en n'arrivant  pas forcément en même  temps. Ainsi
les ressources de transport sont partagées au maximum.
