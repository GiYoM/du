import Criterion.Main


fusion [] ys = ys
fusion xs [] = xs
fusion (x:xs) (y:ys) 
        | x < y = x:(fusion xs (y:ys))
        | otherwise = y:(fusion (x:xs) ys)



main :: IO()
main = defaultMain [
        bgroup "Fusion" [ bench "fusion 50"  $ whnf fusion [1,3..101] [2,4..100]
                           , bench "fib it   500"  $ whnf fusion [1,3..1001] [2,4..1000]
                           , bench "fib memo 5000"  $ whnf fusion [1,3..10001] [2,4..10000]
                       ]
       ]

    