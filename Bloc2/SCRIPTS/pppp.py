from random import randint
import matplotlib.pyplot as plt
from typing import Tuple, NewType, List
from math import inf


Point = NewType('Point', Tuple[int, int])

def echantillon(n: int) -> Tuple[List[int], List[int], List[Point]]:
    Xs: List[int] = []
    Ys: List[int] = []
    Ps: List[Point] = []
    np = 0
    while np < n:
        x, y = randint(0, 2*n), randint(0 ,2*n)
        P = Point((x, y))
        if P not in Ps:
            Ps.append(P)
            Xs.append(x)
            Ys.append(y)
            np += 1
    return Xs, Ys, Ps

def dist2(p1: Point, p2: Point) -> int:
    return (p1[0] - p2[0])**2 + (p1[1] - p2[1])**2

def pp_naif(P: List[Point]) -> Tuple[Point, Point]:
    mini = inf
    n = len(P)
    for i in range(n - 1):
        pi = P[i]
        for j in range(i + 1, n):
            pj = P[j]
            d = dist2(pi, pj)
            if d < mini:
                mini = d
                ppv = (pi, pj)
    return ppv

def pp_bande(bande: List[Point], larg: int) -> Tuple[Point, Point]:
    mini: int = larg
    pm: Tuple[Point, Point] = (bande[0], bande[-1])
    bande.sort(key = lambda p: p[1])
    n: int = len(bande)
    for i in range(n):
        j: int = i + 1
        pi: Point = bande[i]
        while j < n and (pi[1] - bande[j][1]) < mini:
            pj = bande[j]
            dj = dist2(pi, pj)
            if dj < mini:
                mini, pm = dj, (pi, pj)
            j += 1
    return pm



def pp(P: List[Point]) -> Tuple[Point, Point]:
    n = len(P)
    if n <= 3:
        return P[0], P[1]
    Ps: List[Point] = sorted(P)
    pg, pd = pp(Ps[:n//2]), pp(Ps[n//2:])
    dg = dist2(pg[0], pg[1])
    dd = dist2(pd[0], pd[1])
    dm, pm = min((dd, pd), (dg, pg))
    bande: List[Point] = []
    pmil: Point = Ps[n//2]
    for p in Ps:
        if abs(p[0] - pmil[0]) < dm:
            bande.append(p)
    if len(bande) >= 2:
        pb: Tuple[Point, Point] = pp_bande(bande, dm)
        db: int = dist2(pb[0], pb[1])
        d, p = min((dm, pm), (db, pb))
    else:
        p = pm
    return p


    





Xs, Ys, Ps = echantillon(30)

p1, p2 = pp_naif(Ps)
P1, P2 = pp(Ps)

plt.scatter(Xs, Ys)
plt.scatter([p1[0], p2[0]], [p1[1], p2[1]], marker='*', s=300, alpha = 0.5)
plt.scatter([P1[0], P2[0]], [P1[1], P2[1]], marker='^', s=300, alpha = 0.5)
plt.show()
print(dist2(p1, p2))
print(dist2(P1, P2))
