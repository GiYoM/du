import numpy as np
from typing import List
from matplotlib import pyplot as plt
from matplotlib import animation

Pixel = float
Ligne = List[Pixel]
Image = List[Ligne]

#valeurs possibles de sort : "bulles_inverse", "insertion", "bogo", "selection"

def visualisation_tri(n, sort, cmap="inferno", save=False):
    index = 0

    if (save):
        Writer = animation.writers['ffmpeg']
        writer = Writer(fps=60, metadata=dict(artist='Me'), bitrate=1800)

    fig = plt.figure(figsize=(4.5,4.5))
    plt.yticks([])
    plt.xticks([])
    plt.title(sort + ' sort', y=1.02, fontsize=12)
    a= np.random.random((n,n))
    im=plt.imshow(a, cmap = cmap, interpolation='nearest')

    def random_np_array():
        retour = []
        for j in range(n):
            values = []
            for i in range(n):
                values.append(i/(n-1))
            np.random.shuffle(values)
            retour.append(values)
        return np.array(retour)

    def tri_a_bulles_inverse(mat : Image):
        for ligne in mat:
            for y in range(len(ligne)-1):
                if ligne[y] > ligne[y+1]:
                    ligne[y], ligne[y+1] = ligne[y+1], ligne[y]
                    break
        return mat

    def tri_insertion(mat : Image):
        nonlocal index
        for ligne in mat:
            j = index
            while j>0 and j<len(ligne):
                if ligne[j-1] > ligne[j]:
                    ligne[j-1],ligne[j] = ligne[j],ligne[j-1]
                else:
                    break
                j = j-1
        index += 1
        return mat
    
    def est_trie(xs : Ligne):
        est_trie = True
        for q in range(len(xs)-1):
            if xs[q] > xs[q+1]:
                est_trie = False
                break
        return est_trie

    def tri_bogo(mat : Image):
        for k in mat:
            if est_trie(k)==False:
                np.random.shuffle(k)
        return mat


    def tri_selection(mat : Image):
        nonlocal index
        for ligne in mat:
            min_idx = index
            for y in range(index+1, len(ligne)):
                if ligne[min_idx] > ligne[y]:
                    min_idx = y
            ligne[index], ligne[min_idx] = ligne[min_idx], ligne[index]
        if index < len(mat)-1:
            index+=1
        return mat

    switch = {
        "bulles_inverse" : tri_a_bulles_inverse,
        "insertion" : tri_insertion,
        "bogo" : tri_bogo,
        "selection" : tri_selection
    }

    def init():
        im.set_data(random_np_array())
        return [im]


    def animate(i):
        arr=im.get_array()
        a = arr.tolist()
        arr=np.array(switch[sort](a))
        im.set_array(arr)
        return [im]

    switch_frames = {
        "bulles_inverse" : (n**2)//3,
        "insertion" : 2*n,
        "bogo" : n**n,
        "selection" : n**n
    }
    frames=switch_frames[sort]

    anim = animation.FuncAnimation(fig, animate, init_func=init,
                            frames=frames, interval=1, blit=True)
    if (save):
        anim.save(sort + '_'+str(n)+'_'+str(n)+'_'+cmap+'.mp4', writer=writer)

    plt.show()

visualisation_tri(10, "selection")
