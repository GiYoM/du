# Piles


## Un peu d'histoire

En   1920,  le   mathématicien-logicien-philosophe   polonais  Jan   ŁUKASIEWICZ
(1878-1956) invente la notation préfixée alors qu'il est ministre de l'éducation
(on a le droit de rêver...). 

![luka](./IMG/luka.jpg)

35 ans plus tard, le philosophe et informaticien (!) australien (!!!) Charles
HAMBLIN (1922 - 1985) s'en inspire.

Il a en effet en sa possession  l'un des deux ordinateurs présents à l'époque en
Australie. Il  se rend compte  que la saisie de  calculs avec les  opérateurs en
notation  infixée  induit   de  nombreuses  erreurs  de  saisie   par  oubli  de
parenthèses.  Il pense  aussi  à la  (très petite)  mémoire  des ordinateurs  de
l'époque.



Il trouve alors  son inspiration dans le travail de ŁUKASIEWICZ pour
éviter les parenthèses et il
 pense à mettre les opérateurs en position préfixée:
ainsi  il   introduit  la   notion  de   **pile**  (ou   *stack*  ou
*LIFO*) qui économise le nombre d'adresses mémoire nécessaires.


C'est  la  naissance de  la  Notation  Polonaise  Inversée  (NPI ou  RPN).  Elle
économise également la taille des composants électroniques des portes logiques.

Son seul inconvénient:  les mauvaises habitudes prises  d'utiliser des notations
infixées...

Un autre grand avantage: il faut comprendre le calcul avant de l'exécuter :-)


## NPI

```mermaid
graph TD;
DIV -->9;
DIV-->MUL;
MUL-->ADD;
MUL-->SUB;
ADD-->2;
ADD-->NEG;
NEG-->3;
SUB-->6;
SUB-->4;

```


```mermaid
graph TD;
DIV -->9;
DIV-->MUL;
MUL-->NEG;
MUL-->2;
NEG-->1;
```


```mermaid
graph TD;
DIV -->9;
DIV-->NEG;
NEG-->2;
```




```mermaid
graph TD;
NEG-->4.5;
```



```mermaid
graph TD;
-4.5;
```




* Pile
* Stack
* LIFO
* DEPS
* empiler/dépiler
* push/pop


![trainKnuth](./IMG/trainKnuth.png)
![shuntDijkstra](./IMG/shuntDijkstra.png)



## Classe Pile


Barrière d'abstraction : différence entre interface et implémentation.

Voici une première version sans typage (on verra cela avec la classe `Arbre` et
vous pourrez modifier a posteriori).

```python
class Noeud:
    """Un noeud a une valeur et pointe vers un autre noeud"""

    def __init__(self,elt = None, sui = None):
        self.__valeur  = elt
        self.__suivant = sui

    def get_valeur(self):
        """accesseur"""
        return self.__valeur
    
    def set_valeur(self,v):
        """mutateur"""
        self.__valeur = v

    def get_suivant(self):
        return self.__suivant

    def set_suivant(self,s):
        self.__suivant = s

    def  __repr__(self):
        """ on affiche seulement la valeur du noeud """
        return str(self.get_valeur())
		
		
class Pile:
    
    def __init__(self):
        self.__tete = None
        
    def est_vide(self):
        return self.__tete is None

    def empile(self,elt):
        """La tête a pour valeur la valeur entrée et pointe vers l'ancienne tête"""
        n = Noeud(elt)
        n.set_suivant(self.__tete)
        self.__tete = n
        
    def depile(self):
        """On enlève la tête de la pile et on retourne la valeur de la tete"""
        assert not self.est_vide(), "La pile vide ne peut pas être dépilée"
		t = self.__tete
		v = self.val_tete()
		self.__tete = t.get_suivant()
		return v 
            
    def __repr__(self):
        return f"{self.__tete.get_valeur()}|-"
```

Vous remarquerez  que sans typage, le  code est parfois difficile  à suivre. Une
idée d'activité serait de typer ces méthodes.



Voici une seconde version qui utilise les listes `python`:

```python
class Pile:

    p = []
	
	def est_vide(self):
        return self.p == []

    def dépile(self):
		assert not self.est_vide(), "La pile vide ne peut pas être dépilée"
        return self.p.pop()
    
    def empile(self, val):
        self.p.append(val)

	def __repr__(self):
        return f"{p[-1]}|-"

```


Mais **quelque soit l'implémentation, l'interface est la même**.

Ainsi par exemple, que ce soit l'une ou l'autre implémentation, on obtient:

```python
In [124]: p = Pile()

In [125]: p
Out[125]: None|-

In [126]: p.empile(1)

In [127]: p.empile(2)

In [128]: p
Out[128]: 2|-

In [129]: p.empile(3)

In [130]: p
Out[130]: 3|- 

In [131]: p.depile()
Out[131]: 3

In [132]: p
Out[132]: 2|-

In [133]: p.depile()
Out[133]: 2

In [134]: p
Out[134]: 1|-

In [135]: p.depile()
Out[135]: 1

In [136]: p
Out[136]: None|-

In [137]: p.depile()
---------------------------------------------------------------------------
ValueError: La pile vide ne peut pas être dépilée
```



## HP15C

![hp15c](./IMG/hp15c.png)

$`9/((2+\lnot 3)\times (4- \lnot 5))`$

est entré:

`9 2 3 NEG + 4 5 NEG - * /`



* On lit l'expression de gauche à droite;
* On empile les opérandes;
*   Dès qu'on
lit un  opérateur, on l'applique  aux opérandes présents  sur la pile  selon son
arité;
* On s'arrête quand on n'a plus rien à lire;
* on renvoie le dernier élément présent dans la pile.

Avantages:

* pas besoin de parenthèses, de règles d'associativité, de priorité;
* on voit au sommet de la pile les résultats intermédiaires;
* le calcul est directement implémentable sur machine.


## Piles, Files, parcours de graphes

Une  file est  une  structure similaire  à  une pile  mais  fonctionnant sur  le
principe du  "Premier Arrivé  Premier Sorti"  (PAPS) ou  plus internationalement
FIFO (*queue*).

**Comment, à l'aide de la classe `Pile`, créer une classe `File`?**

Vous devez obtenir quelque chose comme ça :

```python
In [18]: f = File()

In [19]: f.enfile(1)

In [22]: f.enfile(2)

In [23]: f.defile()
Out[25]: 1

In [26]: f.defile()
Out[26]: 2

In [27]: f.defile()
-------------------------------------------------
AssertionError: La file vide ne peut être defilée
```



### Parcours en largeur d'un graphe

```
INITIALISATION
Q <- {s}
Marquer s
BOUCLE
Répéter
  Enlever de Q le plus vieux sommet: x
  POUR tout successeur y de x non marqué
      Marquer y
	  Ajouter y en fin de Q
  FIN POUR
Jusqu'à ce que Q soit vide
```

```mermaid
graph TD;
1-->2;
1-->3;
1-->10;
2-->1;
2-->4;
2-->5;
3-->4; 
10-->11;
10-->12;
4-->2;
4-->5;
4-->13;
5-->1;
5-->2;
12-->11;
7-->8;
8-->7;
9;
6-->4;
```

Voir cette [animation du parcours en largeur](./GraphLargeur.pdf)



### Parcours en profondeur d'un graphe

```
INITIALISATION
P <- {s}
Marquer s
BOUCLE
Répéter
  Dépiler de P le dernier sommet stocké : x
  POUR tout successeur y de x non marqué
      Marquer y
	  Empiler y dans P
  FIN POUR
Jusqu'à ce que P soit vide
```

```mermaid
graph TD;
1-->2;
1-->3;
1-->10;
2-->1;
2-->4;
2-->5;
3-->4; 
10-->11;
10-->12;
4-->2;
4-->5;
4-->13;
5-->1;
5-->2;
12-->11;
7-->8;
8-->7;
9;
6-->4;
```

Voir cette [animation du parcours en profondeur](./GraphProfondeur.pdf)






# EXERCICES

## Flavius Josèphe

Flavius Josèphe (37  - 100) ou plutôt Yossef  ben Matityahou HaCohen est
un  historien romain  d'origine juive  et de  langue grecque  et  qui ne
devait pas être trop mauvais en  mathématiques si on en croit la sinistre
anecdote suivante. Lors de  la première guerre judéo-romaine, Yossef fut
piégé dans une grotte avec 39 autres de ses compagnons en juillet 67. Ne voulant pas
devenir esclaves, ils mirent  au point un algorithme d'auto-destruction:
il s'agissait de se mettre en cercle et de se
numéroter de 1 à 40. 

Chaque  septième devait être tué  jusqu'à ce qu'il
n'en  reste plus  qu'un qui  devait alors  se suicider.  Ce  dernier fut
Yossef lui-même...et il ne se suicida  pas! Après deux ans de prison, il
fut libéré, il  entra au service des romains  comme interprète et acquit
la citoyenneté romaine deux ans plus tard.



On voudrait savoir quel numéro portait Yossef. Vous allez créer un programme Python qui donne l'ordre
des exécutions quelque soit le nombre de prisonniers et le nombre fatal.


Dans une  autre situation (pour  ne pas  vous donner la  réponse quand
même...),  on  doit obtenir  avec  20  compagnons  et 6  comme  nombre
sinistre suite:

```python
La 1e victime est le no 6
La 2e victime est le no 12
La 3e victime est le no 18
La 4e victime est le no 4
La 5e victime est le no 11
La 6e victime est le no 19
La 7e victime est le no 7
La 8e victime est le no 15
La 9e victime est le no 3
La 10e victime est le no 14
La 11e victime est le no 5
La 12e victime est le no 17
La 13e victime est le no 10
La 14e victime est le no 8
La 15e victime est le no 2
La 16e victime est le no 9
La 17e victime est le no 16
La 18e victime est le no 13
La 19e victime est le no 1
Out[62]: 'Et le survivant est le no 20'

```



## Entretien Google


Dans cet exercice on considère des chaînes de caractères avec $3$ types  de parenthèses () [] {} (par exemple s`={([[]]))}[)`.


1.   Parmi  les   chaînes  de   caractères  suivantes,   lesquelles  sont   bien
   parenthésées ?
   
* `()[]\{()\}`
* `([(]))`
* `([][]\{()\}` 
* `([][]\{()\})`


2. Écrire une fonction  qui prend en argument une telle  chaîne de caractères et
   vérifie qu'elle est bien parenthésée, c'est-à-dire qui renvoie `True` si elle
   est bien parenthésée et `False` sinon. 
3. Indiquer votre complexité en temps et en espace.
4. Comment simplifier l'algorithme dans le cas  où il n'y aurait qu'un seul type
   de parenthèses ? Quelle est alors la complexité en temps et en espace ? 
5. Dans  l'épreuve de Google,  on suppose disposer  de $N$ types  de parenthèses
   différentes. Comment traiter ce cas ?
   
   
## Création de nouvelles méthodes

On dispose  d'une classe `Pile`  avec les 3 méthodes  vues plus haut.  Il s'agit
d'en créer de nouvelles. 

Dans chaque cas:

* Vous indiquerez pour chaque fonction la complexité en temps et en espace.
* Certaines  fonctions supposent que la  pile est non-vide. On  ne vérifiera pas
  que la pile est bien non-vide et  on laissera l'erreur survenir si la pile est
  en fait vide.
* Vous veillerez à bien faire la différence entre modifier une pile existante et
  créer une nouvelle pile. 
*  Vous  n'utiliserez aucune  autre  structure  de  données  que les  piles.  En
  particulier, vous n'utiliserez pas de listes. 
  
Implémenter les fonctions suivantes sur les piles:
  
1. `p.nonepop()` qui  a le même effet  que `p.dépile()`, sauf si  la pile est
   vide : dans ce cas, la pile reste inchangée et `p.nonepop()` renvoie `None`. 
2. `p.duplique()` qui duplique l'élément au sommet de la pile.
3. `p.echange()` qui échange les deux premiers éléments de la pile `p`.
4. `p.ecrase()` qui efface tous les éléments de `p`.
5. `p.inverse ()` qui inverse l'ordre des éléments de `p`.
6. `p.copy ()` qui renvoie une nouvelle pile, qui est une copie de `p`




## Créer sa HP15C

Créez une calculatrice  qui effectue les calculs  arithmétiques à la
  manière d'une HP15C.

  On  pourra se  contenter d'une  version simplifiée:  on donne  comme
  argument  la chaîne  correspondant  à l'opération  à effectuer  sous
  forme postfixée. La fonction renvoie le résultat de l'opération. Par
  exemple:
  
  ```python
In [1]: seq = "2 3 4 + *"

In [2]: npi(seq)
Out[2]: 14
  ```
  

On pourra utiliser les fonctions du module `operator`.

Par exemple, `operator.add(2,3)` renvoie 5.

On peut alors créer un dictionnaire des opérations:


```python
operations = {
    '+':  operator.add, '-':  operator.sub,
    '*':  operator.mul, '/':  operator.truediv,
    '%':  operator.mod, '**': operator.pow,
    '//': operator.floordiv
}
```


Vous   pouvez    ensuite   proposer   une   calculatrice    plus 
interactive:

```python
In [1]: hp_inter()
->  |
rentrer un nombre ou un opérateur: 1
-> 1 |
rentrer un nombre ou un opérateur: 2
-> 2, 1 |
rentrer un nombre ou un opérateur: 3
-> 3, 2, 1 |
rentrer un nombre ou un opérateur: 4
-> 4, 3, 2, 1 |
rentrer un nombre ou un opérateur: '+'
-> 7, 2, 1 |
rentrer un nombre ou un opérateur: '-'
-> -5, 1 |
rentrer un nombre ou un opérateur: '+'
-> -4 |
rentrer un nombre ou un opérateur: 3
-> 3, -4 |
rentrer un nombre ou un opérateur: '*'
-> -12 |
rentrer un nombre ou un opérateur: 'fin'
```
