# Étude d'un algorithme ou comment faire de l'informatique sans ordinateur...


## L'expérience


![Lancer d'actionnaires](./IMG/hudsucker.JPG)


* *k* actionnaires
* $`N=2^n`$ étages
* RDC  = 0
* Il existe un étage fatal
* RDC non fatal
* Minimiser le nombre d'essais



*  Première idée:  on commence  au  rez-de-chaussée et  on progresse  d'un
    étage.
* Combien d'essais au pire?
* En moyenne ?


## Diviser pour régner


```shell
inf ⟵ Étage inférieur
sup ⟵ Étage supérieur
milieu ⟵  (inf + sup)/2
if estFatal(milieu)
   ChercherEntre(inf,milieu)
else
   ChercherEntre(milieu, sup)
done
```


```shell
FONCTION Chercher_Entre(inf, sup : entiers) -> entier
# pré-condition: il existe au moins un étage fatal entre inf et sup
# invariant: le plus petit étage fatal est entre inf et sup
# post-condition: la valeur retournée est le plus petit étage fatal
if sup == inf
   return sup
else 
   inf ⟵ Étage inférieur
   sup ⟵ Étage supérieur
   milieu ⟵ (inf + sup) // 2
   if estFatal(milieu)
      ChercherEntre(inf,milieu)
   else
      ChercherEntre(milieu, sup)
   done
done
```

```shell
FONCTIOΝ ChercherEntre(N: entier) -> entier
# pré-cond.: il existe un étage fatal entre inf non compris et sup, inf < sup
# invariant: le plus petit étage fatal est entre inf (non compris) et sup
# post-condition: la valeur retournée est le plus petit étage fatal
inf,sup ⟵ 0, N
while sup > inf + 1 do
   # plus petit étage fatal est entre inf non compris et sup, sup > inf + 1
   milieu ⟵ ( inf + sup ) // 2
   if estFatal(milieu)
      sup = milieu
   else 
      inf = milieu
   done
done
#le plus petit étage fatal est entre inf (non compris) et sup. De plus sup = inf + 1
return sup
# la valeur retournée est le plus petit étage fatal
```


#### QUESTIONS

* étude de la *terminaison* de l'algorithme: est-ce que la fonction
  renvoie effectivement une valeur?
* étude  la  *correction*  de l'algorithme: est-ce que  la fonction
  renvoie la valeur attendue?
*  étude  de la   *complexité*  de  l'algorithme: peut-on  estimer  la
  vitesse d'exécution de cet algorithme?
  

#### RÉPONSES

* **TERMINAISON**:  $`\ell_i=\frac{N}{2^i}=\frac{2^n}{2^i}=2^{n-i} \quad \ell_n=1`$
* **CORRECTION**: l'invariant est vérifié à chaque itération
* **COMPLEXITÉ**:  il suffit donc de compter combien de lancers ont été effectués. La réponse est
  dans l'étude faite  pour prouver la terminaison: c'est à  l'étape *n* que l'on
  atteint la condition de sortie de l'algorithme.
  Que vaut *n*?  
  On a posé au départ que le nombre d'étages était une puissance
  de 2: $`N=2^n`$. Ainsi, $`n=\log_2N`$.



## Et la recherche dichotomique d'une solution d'une équation réelle?

 Cherchons une approximation de $`x^2-2=0`$ par la méthode de dichotomie avec
  une précision de $`2^{-10}`$ entre 1 et 2.  Il va falloir chercher:


* un nombre  (un étage)
* dans un tableau (un immeuble)
* de $`2^{10}`$ nombres (étages)

|$`1`$ | $`1+2^{-10}`$ | $`1+2\times 2^{-10}`$| $`1+3\times 2^{-10}`$|$`\cdots`$|$`1+2^{10}\times 2^{-10}`$|
|------|----------------|------------------------|-----------------------|-----------|----------------------------|






Notre  fonction  booléenne `estFatal`  est alors  le  test  $`x  \mapsto
\mathtt{x*x <= 2}`$ et l'on va chercher  une cellule de ce tableau par dichotomie
comme on cherchait un étage dans un immeuble.


```python
def racineDicho(prec: float) -> Tuple(float, int):
    cpt: int = 0
    inf: float = 1
    sup: float = 2
    while (sup - inf > prec):
        m = inf + (sup - inf) / 2
        cpt += 1
        if m*m <= 2:
            inf = m
        else:
            sup = m
    return sup,cpt
```
  
  
  ```console
In [1]: racineDicho(2**(-10))
Out[1]: (1.4150390625, 10)

In [2]: racineDicho(2**(-15))
Out[2]: (1.414215087890625, 15)

In [3]: racineDicho(2**(-20))
Out[3]: (1.4142141342163086, 20)

In [4]: racineDicho(2**(-30))
Out[4]: (1.4142135623842478, 30)

In [5]: racineDicho(2**(-50))
Out[5]: (1.4142135623730958, 50)
  ```
  
  
## Un peu de maths...l'algorithme de Héron et sa complexité

![table Babylonienne](./IMG/babylone.png)


> Puisque alors  les 720 n'ont  pas le côté  exprimable, nous prendrons  le côté
> avec une très petite différence ainsi. Puisque  le carré le plus voisin de 720
> est 729 et il a 27 comme côté, divise les  720 par le 27 : il en résulte 26 et
> deux tiers.  Ajoute les 27  : il en  résulte 53 et  deux tiers. De  ceux-ci la
> moitié :  il en  résulte 26 2'  3'. Le côté  approché de  720 sera donc  26 2'
> 3'. En effet 26 2'  3' par eux-mêmes : il en résulte 720  36', de sorte que la
> différence est une 36e  part d'unité. Et si nous voulons  que la différence se
> produise par une part  plus petite que le 36', au lieu  de 729, nous placerons
> les  720 et  36' maintenant  trouvés  et, en  faisant les  mêmes choses,  nous
> trouverons la différence qui en résulte inférieure, de beaucoup, au 36'.


* Si  $`x_n`$  est une  approximation  strictement  positive  par défaut  de
$`\sqrt{a}`$, alors $`a/x_n`$ est  une approximation par excès de $`\sqrt{a}`$
 et vice-versa.

* La moyenne arithmétique de ces deux approximations est
$`\frac{1}{2}\left(x_n+\frac{a}{x_n}\right)`$  et constitue  une meilleure
approximation que les deux précédentes.

* On peut montrer que c'est une approximation par excès (en développant
$`\left(x_n-\sqrt{a}\right)^2`$ par exemple).


```console
FONCTIOΝ heron_rec(a,fo: Rationnels, n: Entier) → Rationnel
   Si n = 0
      retourner 0
   Sinon
      Retourner heron_rec(a,(fo + a / fo) / 2, n - 1)
   FinSi

FONCTION heron_it(a,fo: Rationnels, n: Entier → Rationnel
   app ← 0
   Pour k de 0 jusqu'à n - 1 faire
        app ← (app + a / app) / 2
   FinPour
   Retourner app
```


#### Est-ce  que la  suite des  valeurs calculées  par la  boucle converge  vers
#### $`\sqrt{2}`$?



>**Théorème de la limite monotone**
>    Toute suite croissante majorée (ou décroissante minorée) converge.


> **Théorème light du point fixe**
>  Soit I un intervalle fermé de $`\mathbb R`$, soit *f* une fonction continue de Ι vers I
>  et soit $`(r_n)`$ une suite d'éléments de I telle que $`r_{n+1}=f(r_n)`$ pour tout
>  entier naturel  *n*. SI $`(r_n)`$ est  convergente ALORS sa limite  est UΝ point
>  fixe de *f* appartenant à I.


$`r_{n+1}= f(r_n)=\frac{r_n + \frac{2}{r_n}}{2}\qquad r_0=1`$


*  pour tout entier naturel non nul *n*, on a $`r_n \geqslant \sqrt{2}`$;
* la suite est décroissante;
* $`\sqrt{2}`$ est l'unique point fixe positif de *f*.


#### Quelle est la vitesse de convergence?

> **Ordre d'une suite**
>   Soit $`(r_n)`$ une  suite convergeant vers $`\ell`$. S'il existe  un entier $`k>0`$
>   tel que: 
>	$`\displaystyle\lim_{n\to>+\infty}\frac{|r_{n+1}-\ell|}{|r_n-\ell|^k}=C `$ 
>
> avec $`C\neq 0`$ et $`C\neq +\infty`$. 
>
> On dit que $`(r_n)`$ est d'ordre *k* et que C est la constante
>   asymptotique d'erreur. 


$`\left|r_{n+1} - \sqrt{2}\right|  = \left| \frac{(r_n-\sqrt{2})^2}{2r_n}\right|
`$

$`  \left|r_{n+1}  -
  \sqrt{2}\right| \leqslant \left| (r_n-\sqrt{2})^2\right| `$

$`d_n=-\log_{10}|r_n-\sqrt{2}| \qquad d_{n+1} \geqslant 2d_n`$


