from pile import *

def liste2pile(xs) :
    p = Pile()
    for x in xs :
        p.empile(x)
    return p


#############################
#   Barrière d'abstraction  #
#############################

def nonepop(p) :
    if p.est_vide() :
        return None
    return p.depile()
    
def duplique(p) :
    sommet = p.depile()
    p.empile(sommet)
    p.empile(sommet)
    return None
    
def ecrase(p) :
    while not p.est_vide() :
        p.depile()
    return None
    
def echange(p) :
    som1 = p.depile()
    som2 = p.depile()
    p.empile(som1)
    p.empile(som2)
    return None
    
def copy(p) :
    cp = Pile()
    tmp = Pile()
    while not p.est_vide() :
        tmp.empile(p.depile())
    while not tmp.est_vide() :
        som = tmp.depile()
        cp.empile(som)
        p.empile(som)
    return cp

def inverse(p) :
    cp = copy(p)
    ecrase(p)
    while not cp.est_vide() :
        p.empile(cp.depile())
    return None
    
def pile2liste(p) :
    cp = copy(p)
    inverse(cp)
    xs = []
    while not cp.est_vide() :
        xs.append(cp.depile())    
    return xs
    
def longueur(p) :
    cp = copy(p)
    long = 0
    while not cp.est_vide() :
        cp.depile()
        long += 1
    return long
