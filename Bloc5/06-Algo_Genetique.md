# Optimisation et aléatoire : algorithmes génétiques

Les algorithmes génétiques s'inspirent de  la théorie de l'évolution en simulant
l'évolution d'une population. Ils font intervenir cinq traitements.

1. **Initialisation**
   Il s'agit  de créer une  population d'origine composée  de 𝑚 individus  (ici des
   chemins pour  l'exploration à planifier).  Généralement la population  de départ
   est produite aléatoirement.
2. **Évaluation**
   Cette étape consiste à attribuer à chaque individu de la population courante une
   note correspondant à sa capacité à répondre au problème posé.
3. **Sélection**
   Une  fois  tous  les  individus   évalués,  l'algorithme  ne  conserve  que  les
   « meilleurs » individus. Plusieurs méthodes  de sélection sont possibles : choix
   aléatoire, ceux qui ont obtenu la meilleure note, élimination par tournoi, etc.
4. **Croisement**
   Les individus  sélectionnés sont croisés deux  à deux pour produire  de nouveaux
   individus  et  donc une  nouvelle  population.  La  fonction de  croisement  (ou
   reproduction) dépend de la nature des individus.
5. **Mutation**
   Une proportion  d'individus est choisie (généralement  aléatoirement) pour subir
   une  mutation, c'est-à-dire  une  transformation aléatoire.  Cette étape  permet
   d'éviter à l'algorithme de rester bloqué sur un optimum local.

En répétant les  étapes de sélection, croisement et  mutation, l'algorithme fait
ainsi  évoluer la  population,  jus  qu'à trouver  un  individu  qui réponde  au
problème initial. Cependant dans les cas pratiques d'utilisation des algorithmes
génétiques, il n'est pas possible de savoir simplement si le problème est résolu
(le plus  court chemin figure-t-il  dans ma population  ?). On utilise  donc des
conditions d'arrêt heuristiques basées sur un critère arbitraire.


Étudiez par exemple la partie III de ce problème proposé au concours Centrale-Supélec de
[déplacement d'un robot sur Mars](./CentraleMars.pdf)



## Proposition de solution (à ne pas regarder tout de suite...)


```python

# III.A Initialisation et évaluation


# ----------------------------------

# La seule question est d'utiliser random.shuffle, random.sample ou de
# programmer soi-même une permutation aléatoire. Les deux fonctions
# figurant dans l'annexe, cela serait une perte de temps de les
# reprogrammer.
# Cette implantation utilise random.sample dont l'intérêt est de pouvoir
# directement s'appliquer sur range(n)

def créer_population_v1(m:int, d:np.ndarray) -> list:
    """
    Crée une population aléatoire de m individus.
    
    >>> d = calculer_distances_v1(np.array([[1, 1], [1, 2], [0, 1], [1, 0]]))
    >>> créer_population_v1(3, d) # doctest: +ELLIPSIS
    [(...)]
    """
    n = len(d) - 1 # nombre de points d'intérêt (sans la position du robot)
    
    popu = []
    for i in range(m):
        chemin = random.sample(range(n), n)
        popu.append((longueur_chemin(chemin, d), chemin))
    return popu

# Cette implantation utilise random.shuffle qui nécessite de construire
# d'abord une liste car le résultat de range(n) n'est pas mutable.

def créer_population_v2(m:int, d:np.ndarray) -> list:
    """
    Crée une population aléatoire de m individus.
    
    >>> d = calculer_distances_v1(np.array([[1, 1], [1, 2], [0, 1], [1, 0]]))
    >>> créer_population_v2(3, d) # doctest: +ELLIPSIS
    [(...)]
    """
    n = len(d) - 1 # nombre de points d'intérêt (sans la position du robot)
    
    popu = []
    for i in range(m):
        chemin = list(range(n))

        random.shuffle(chemin)
        popu.append((longueur_chemin(chemin, d), chemin))
    return popu


# III.B Sélection# ---------------

# Il faut comprendre que la fonction doit modifier la liste. Ce qui est
# clairement indiqué dans l'énoncé et souligné par le fait que la
# fonction n'a pas de résultat.

# L'énoncé ne précise pas que faire dans le cas où la population
# initiale ne compte pas un nombre pair d'individus. Le choix fait ici


# est d'arrondir à l'entier supérieur le nombre d'individus conservés.

# L'algorithme utilisé ici consiste à trier à liste dans l'ordre
# croissant des individus, ce qui correspond à l'ordre croissant des
# longueurs de chemins et de supprimer ensuite la deuxième moitié
# correspondant aux chemins les plus longs. En cas d'ex æquo on conserve
# les chemins qui commencent par les PI avec les plus petits indices
# (conséquence du tri dans l'ordre lexicographique)

def réduire(p:list) -> None:
    """
    Sélection de la moitié des meilleurs individus.
    
    >>> d = calculer_distances_v1(np.array([[1, 1], [1, 2], [0, 1], [1, 0]]))
    >>> p = créer_population_v1(3, d)
    >>> réduire(p)
    >>> len(p)
    2
    >>> p = créer_population_v1(4, d)
    >>> réduire(p)
    >>> len(p)
    2
    """
    p.sort() # trier par longueur de chemin
    m = len(p) # effectif de la population
    m = math.ceil(m/2) # effectif moitié

    del p[m:] # on conserve seulement les m premiers (meilleurs) individus


# III.C.1 Muter un chemin
# -----------------------

# On prend deux éléments au hasard et on les inverse. Il faut bien sûr
# que le chemin ait au moins deux éléments.
# Autre écueil, les deux éléments choisis doivent être distincts. Sinon
# la mutation ne fait rien !

def muter_chemin(c:list) -> None:
    """
    Transforme une liste en permuttant deux éléments choisis aléatoirement
    
    >>> muter_chemin([1, 2, 3, 4])
    """
    n = len(c) # nombre de points d'intérêt
    assert n >= 2

    i = random.randrange(0, n)
    j = random.randrange(0, n)
    if i == j:
        j = (i + 1) % n
    c[i], c[j] = c[j], c[i]


# III.C.2 Muter une population
# ----------------------------

# La seule question est d'interpréter correctement la probabilité de
# mutation d'un individu. Une première approche est de calculer le
# nombre d'individus à muter en multipliant la probabilité de mutation
# d'un individu par l'effectif de la population, puis de tirer au sort
# les individus en nombre indiqué. Mais on réduit ainsi considérablement
# l'aléa, surtout pour des populations faibles. Une autre approche est
# de passer en revue chaque individu et de tirer au sort avec la
# probabilité indiquée s'il doit muter. C'est cette deuxième approche qui
# est implantée ici.

def muter_population(p:list, proba:float, d:np.ndarray) -> None:
    """
    Fait muter des individus dans une population.
    
>>> d = calculer_distances_v1(np.array([[1, 1], [1, 2], [0, 1], [1, 0]]))
>>> p = créer_population_v1(3, d)
>>> muter_population(p, 0.1, d)

    """
    for i in range(len(p)):
        if random.random() > proba: continue
        longueur, chemin = p[i]
        muter_chemin(chemin)
        p[i] = longueur_chemin(chemin, d), chemin



# III.D.1 Croiser deux chemins
# ----------------------------

# La marche à suivre est indiquée dans le sujet. On peut se poser la
# question de comment traiter le cas d'un nombre de points impair, mais
# quelle que soit la solution choisie, la normalisation finale permet
# d'obtenir un chemin valide avec le bon nombre de points.

def croiser(c1:list, c2:list) -> list:
    """
    Crée un nouveau chemin en prenant le début du premier et la fin du second.
    
    >>> croiser([0, 1, 2], [1, 2])
    Traceback (most recent call last):
        ...
    AssertionError
    >>> croiser([0, 2, 4, 1, 3], [3, 1, 4, 2, 0])
    [0, 2, 4, 1, 3]
    >>> croiser([0, 1, 3, 2], [3, 2, 1, 0])
    [0, 1, 2, 3]
    """

    assert len(c1) == len(c2)
    n = len(c1)
    coupure = math.ceil(n/2)
    return normaliser_chemin_v1(c1[:coupure] + c2[coupure:], n)


# III.D.2 Nouvelle génération
# ---------------------------

# Il faut modifier la population sans perturber le fonctionnement de
# l'algorithme. Un append fait cela très bien puisqu'il ne touche pas
# aux premiers éléments.



def nouvelle_génération(p:list, d:np.ndarray) -> None:
    """

Ajoute les enfants de la population courante.
    
    >>> d = calculer_distances_v1(np.array([[1, 1], [1, 2], [0, 1], [1, 0]]))
    >>> p = créer_population_v1(3, d)
    >>> nouvelle_génération(p, d)
    >>> len(p)
    """
    m = len(p)
    for i in range(m):
        l1, c1 = p[i]
        l2, c2 = p[(i+1) % m]
        c = croiser(c1, c2)
        p.append((longueur_chemin(c, d), c))


# III.E.1 Fonction complète
# -------------------------

def algo_génétique(PI:np.ndarray, m:int, proba:float, g:int) -> (float, list):
    """
    >>> PI = np.array([[1, 1], [1, 2], [0, 1], [2, 2]])
    >>> algo_génétique(PI, 10, 0.1, 100)
    (4.0, [2, 0, 1, 3])
    """
    # Initialisation
    d = calculer_distances_v1(PI)
    p = créer_population_v1(m, d)
    
    # On itère l'algo g fois
    for i in range(g):
        réduire(p)
        nouvelle_génération(p, d)
        muter_population(p, proba, d)
    
    # Résultat
    return min(p)


# III.E.2 Non régression
# ----------------------

# Les fonctions réduire et nouvelle_génération sont assurées de
# conserver le meilleur individu dans la population: réduire par
# construction et nouvelle_génération puisqu'elle ne modifie pas les
# individus existants. En revanche il peut arriver que la fonction
# muter_population fasse muter le meilleur individu vers un chemin plus
# long. En conséquence, rien n'assure avec l'implantation proposée que
# la génération n+1 ne soit pas plus mauvaise que la génération n.

# Pour éviter cela, il suffit de s'assurer de ne pas muter le meilleur
# individu. Une solution simple est de remarquer que le meilleur
# individu de la génération est le premier élément de la liste d'individus
# reçue par la fonction muter_population. Ainsi commencer la boucle de
# cette fonction à l'individu d'indice 1 au lieu de 0 permet de garantir
# la non régression.

def muter_population_v2(p:list, proba:float, d:np.ndarray) -> None:
    """
    Fait muter des individus dans une population, sauf le premier.
    
    >>> d = calculer_distances_v1(np.array([[1, 1], [1, 2], [0, 1], [1, 0]]))
    >>> p = créer_population_v1(3, d)
    >>> muter_population_v2(p, 0.1, d)
    """
    for i in range(1, len(p)):
        if random.random() > proba: continue
        longueur, chemin = p[i]
        muter_chemin(chemin)
        p[i] = longueur_chemin(chemin, d), chemin


# III.E.3 Conditions d'arrêt
# --------------------------

# En plus du nombre de générations, on peut imaginer d'utiliser
# - le temps écoulé
# - un critère de convergence
# - un mélange (convergence + temps maxi)

# Par rapport au nombre de génération, le temps écoulé présente
# l'intérêt de pouvoir s'ajuster plus facilement aux paramètres
# opérationnels du robot (durée d'ensoleillement restante, impératif de
# déplacement à partir de telle heure, etc.).

# Un critère de convergence n'est pas évident à mettre en place. Si on
# ne considère que le meilleur individu, on risque de s'arrêter sur un
# optimum local. D'autre part, ce type d'algorithme progresse
# généralement par saut, il faut donc éviter de prendre un plateau pour
# le résultat final. On peut par exemple considérer que s'il n'observe
# pas d'amélioration au bout d'un certain nombre de générations
# successives, le programme s'arrête.
```
