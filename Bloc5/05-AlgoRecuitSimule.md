# Optimisation et aléatoire : recuit simulé

Le recuit  simulé est une méthode  utilisée en metallurgie pour  rendre un métal
plus "harmonieux"  vis-à-vis de sa distribution  atomique ce qui le  rendra plus
formable. Un métal est chauffé: l'énergie  permet de casser des liaisons. Il est
ensuite refroidi  très lentement: les  atomes forment de nouvelles  liaisons qui
sont souvent plus régulières.

Le mécanisme repose sur la distribution de probabilité de Boltzmann:

$`p(E)\propto \exp(-E/kT)`$

La probabilité qu'un  système passe d'un niveau d'énergie $`E_1`$  à $`E_2`$ est
donc 

$`p=
\begin{cases}
\exp((E_1-E_2)/kT) \text{ si } E_2>E_1\\
1 \text{ sinon}\\
\end{cases}
`$

Si l'énergie baisse, la configuration est conservée systématiquement.

Dans le cas contraire, elle est  conservée avec une probabilité qui diminue avec
la température.

En  1953, cette  méthode a  été généralisée  au calcul  numérique dans  l'article
«  Equations of  State Calculations  by Fast  Computing Machines  » de  Nicholas
Metropolis, Arianna  W. Rosenbluth, Marshall  Rosenbluth, Augusta H.  Teller, et
Edward Teller.


L'algorithme porte le nom  de Metropolis (au quel on adjoint  le nom de Hastings
qui en 1970 a généralisé la  méthode à d'autres distributions de probabilité que
celles de Boltzmann) ou plus communément *recuit-simulé*

```console
FONCTION Metropolis-Hastings(x)
    y ← y0
	T ← T0
    TANT QUE T n'est pas froid FAIRE
	    y_candidat ← y + ∆y
		∆E ← E (x | y_candidat) − E (x | y)
		SI exp (− ∆E/T ) ≥ nb uniformément choisi dans [0,1]
             y ← y_candidat
	    FIN_SI
        T ← T diminué
	RETOURNER y
```


Le petit  saut probabiliste permet  de s'extraire de  minima locaux au  début de
l'exploration afin de ne pas rester coincé dans un minimum local. 


## Allocation des niveaux de vol et recuit-simulé

Voici  un  autre  problème  de Centrale-Supélec  étudiant  les  [allocations  de
niveaux de vol](./CentraleVol.pdf). On n'étudiera que les partiees I et II.



Voici une proposition de corrigé:

```python
import numpy as np

conflit = [[0,0,0,100,100,0,0,150,0],
           [0,0,0,0,0,50,0,0,0],
           [0,0,0,0,200,0,0,300,50],
           [100,0,0,0,0,0,400,0,0],
           [100,0,200,0,0,0,200,0,100],
           [0,50,0,0,0,0,0,0,0],
           [0,0,0,400,200,0,0,0,0],
           [150,0,300,0,0,0,0,0,0],
           [0,0,50,0,100,0,0,0,0]]

npconf=np.array(conflit)

# II Α 1

def nb_conflits() :
    cpt = 0
    n = len(conflit)
    for i in range(n - 3) :
        for j in range(i + 3 - (i % 3), n) :
            cpt += conflit[i][j] > 0
    return cpt

# II Β 1

def nb_vol_par_niveau_relatif(regulation) :
    a, b, c = 0, 0, 0
    for r in regulation :
        a += r == 0
        b += r == 1
        c += r == 2
    return [a, b, c]

def nb_vol_par_niveau_relatif2(regulation) :
    res = [0, 0, 0]
    for k in regulation:
        res[k] += 1
    return res

# IΙ B 2 a

def cout_regulation(regulation):
    n = len(regulation)
    cout = 0
    for i in range(0, 3*(n - 1), 3):
        for j in range(i + 3, 3*n, 3):
            cout += conflit[i + regulation[i//3]][j + regulation[j//3]]
    return cout

def cout_regulation2(regulation):
    cout = 0
    for no_vol1 in range(len(regulation)):
        for no_vol2 in range(no_vol1):
            cout += conflit[3*no_vol1 + regulation[no_vol1]][3*no_vol2 + regulation[no_vol2]]
    return cout

def cout_RFL():
    return cout_regulation((len(conflit) // 3) * [0])

# II C 1

def cout_du_sommet(s, etat_sommet):
    cout = 0
    n = len(etat_sommet)
    for j in range(n):
        cout += conflit[s][j] if etat_sommet[j] > 0 else 0
    return cout

# II C 2 a

def sommet_de_cout_min(etat_sommet) :
    s = 0
    n = len(etat_sommet)
    while etat_sommet[s] < 2 and s < n - 1 :
        s += 3
    s_mini, mini = s, cout_du_sommet(s,etat_sommet)
    while s < n - 2 :
        if etat_sommet[s] == 2 : 
            for i in range(3) :
                c = cout_du_sommet(s + i, etat_sommet)
                if c < mini :
                    s_mini, mini = s + i, c
        s += 3
    etat_sommet[3*(s_mini//3):3*(s_mini//3)+3] = [0]*3
    etat_sommet[s_mini] = 1
    return s_mini, etat_sommet
            
# IΙ C 3

def minimal() :
    n = len(conflit)
    etat_sommet = [2]*n
    regulation = [-1]*(n//3)
    for k in range(n//3) :
        s, etat_sommet = sommet_de_cout_min(etat_sommet)
        regulation[s//3] = s%3
    return regulation
    
    
# II D

def recuit(regulation) :
    T = 1000
    while T >= 1 :
        k = np.random.randint(len(regulation))
        r = regulation[k]
        c = cout_regulation(regulation)
        rs = [i for i in range(3) if i != r]
        new_reg = regulation[:]
        nr = rs[np.random.randint(2)]
        new_reg[k] = nr
        new_c = cout_regulation(new_reg)
        if new_c == 0 :
            return new_reg
        if new_c < c or np.random.rand() < np.exp(-(new_c - c)/T) :
            regulation[k] = nr
            print(new_c)
        T *= 0.99
    return regulation
```



