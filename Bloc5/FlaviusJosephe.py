from functools import reduce


##############################################################
#
#   LΕ MOTEUR
#
#############################################################

# def tete(xs):
#     return xs[0]

# def queue(xs):
#     return xs[1:]

# Vide = []

# def estVide(xs):
#     return xs == Vide

# def insere(x,xs):
#     return [x] + xs
Vide = None

# testeur

def estVide(lst):
    """
    teste si une liste est vide
    """
    return lst == Vide

# sélecteurs

def tete(lst):
    """
    renvoie le premier élément d'une liste (sa tête)
    
    >>> tete((1,(2,(3,vide))))
    1
    """
    if estVide(lst):
        raise ValueError("La liste vide n'a pas de tête") 
    else:
        return lst[0]

def queue(lst):
    """
    renvoie la liste privée de sa tête : c'est encore une liste
    
    >>> queue((1,(2,(3,vide))))
    (2, (3, vide))
    """
    if estVide(lst):
        raise ValueError("La liste vide n'a pas de queue") 
    else:
        return lst[1]

# insertion
    
def insere(el,lst):
    """
    insere un élement en tête de liste
    
    >>> insere(12, (1,(2,(3,vide))))
    (12, (1, (2, (3, vide))))
    """
    return (el,lst)


##############################################################
#
# The BARRIÈRE D'ABSTRACTION : derrière cette barrière, on n'utilise
# que les fonctions qui viennent d'être définies.
#
##############################################################

def concat(lst1,lst2):
    """
    concatène deux listes

    >>> concat((0,(1,(2,(3,(4,vide))))) ,(10,(11,(12,(13,(14,vide))))) )
    (0, (1, (2, (3, (4, (10, (11, (12, (13, (14, vide))))))))))
    """
    if estVide(lst1):
        return lst2
    else:
        return insere(tete(lst1), concat(queue(lst1),lst2))



def gonfle(f,loiMonoide,neutreMonoide):
    """
    crée le morphisme de monoïde entre [a] et b si
    i.e. gonfle f : a -> b en f* : [a] -> b
    loiMonoide : (b*b) -> b
    neutreMonoide le neutre de b
    f  traite les éléments
    f* traite les listes de ces éléments
    f*([a])          = f(a)
    f*([as] ++ [as]) = f*([as]) loiMonoide f*([as])
    f*([])           = neutreMonoide

    ex:
    longueur =                    gonfle(lambda x: 1                , operator.add             , 0)
    somme    =                    gonfle(lambda x: x                , operator.add             , 0)
    membre   = lambda x:               gonfle(lambda y: y == x           , lambda acc, k: acc or k       , False )
    applique = lambda g:               gonfle(lambda x: insere(g(x),Vide), lambda acc, gk: concat(gk,acc), Vide)
    plie_g   = lambda combine, neutre: gonfle(lambda x:x                 , combine                  ,neutre)

    """
    def fp(liste):
        if estVide(liste):
            return neutreMonoide
        else:
            return loiMonoide(  fp(queue(liste)), f(tete(liste)))
    return fp

plieG    = lambda combine, neutre: gonfle(lambda x: x, combine, neutre)
applique = lambda g: gonfle(lambda x: insere(g(x),Vide), lambda acc, gk: concat(gk,acc), Vide)


def supprime(i,xs):
    """supprime l'élément no i d'une liste"""
    if estVide(xs):
        raise ValueError("Suppression dans une liste vide")
    if i == 0:
        return queue(xs)
    return insere(tete(xs), supprime(i-1,queue(xs)))

def soldats(n):
    """"construit une liste de chaines No1 jusque No n
        In [117]: soldats(5)
        Out[117]: ['No 1', 'No 2', 'No 3', 'No 4', 'No 5']

    """
    def construit(k,acc):
        if k == 0:
            return acc
        return construit(k - 1, insere(k,acc))
    return applique(lambda x: "No " + repr(x))(construit(n,Vide))
 
def numero(i,xs):
    """renvoie l'élément numéro i d'une liste"""
    if estVide(xs):
        raise ValueError("Une liste vide n'a pas d'élément")
    if i == 0:
        return tete(xs)
    return numero(i-1,queue(xs))


def flaviusRec(nSinistre,nbPers):
    """Version récursive du problème de Flavius"""
    def killing(surv,nbTues,noVictime):
        if estVide(queue(surv)):
            return ("Et le survivant est le " + tete(surv))
        n = (noVictime + nSinistre - 1) % (nbPers - nbTues)
        print("La " + str(nbTues) + "e victime est le " + numero(n,surv))
        return killing(supprime(n,surv),nbTues + 1,n)
    return killing(soldats(nbPers),0,0)

def flaviusImp(nSinistre,nbPers):
    """Version impérative du problème de Flavius"""
    surv      = soldats(nbPers)
    noVictime = 0
    nbTues    = 0
    while nbTues < nbPers - 1:
        noVictime = (noVictime + nSinistre - 1) % (nbPers - nbTues)
        nbTues   += 1
        print("La " + str(nbTues) + "e victime est le " + numero(noVictime,surv))
        surv      = supprime(noVictime,surv)
    return("Et le survivant est le " + tete(surv))


################################################################################
#
#   VERSIONS  PYTHONESQUES
#
################################################################################""



def flaviusPyth(nSinistre,nbPers):
    """Version directement pythonesque du problème de Flavius"""
    surv = ["No " + str(k) for k in range(1,nbPers + 1)]
    noVictime = 0
    while len(surv) > 1:
        noVictime = (noVictime + nSinistre - 1) % len(surv)
        print(surv.pop(noVictime))
    return("Et le survivant est le " + surv[0])


def flaviusPyth2(nSinistre,nbPers):
    """ Par pliage, avec X = (surv,noVictime)"""
    def reduit(X,NbTues):
        print(X[0].pop(X[1]))
        return (X[0], (X[1] + nSinistre - 1) % (nbPers - NbTues))
    return reduce(reduit, range(1,nbPers),  ( ["No " + str(k) for k in range(1,nbPers + 1)],nSinistre - 1) )


def flaviusRec2(nSinistre,nbPers):
    """ Par pliage, avec X = (surv,noVictime)"""
    def reduit(X,NbTues):
        print(numero(X[1],X[0]))
        return (supprime(X[1],X[0]), (X[1] + nSinistre - 1) % (nbPers - NbTues))
    return plieG(reduit, (soldats(nbPers) ,nSinistre - 1) )(range(1,nbPers))

    
