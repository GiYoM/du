# -*- coding: utf-8 -*-

############ Corrigé du sujet réécrit sur le codage de Huffmann
############ (d'après Mines-Ponts 2015)



################################### 1. Réception des données 

#### Le capteur

"""
# Q 1
On a 2^10 = 1024
On code les entiers de l'intervalle [-512, 511] par leur reste modulo 1024
(il est compris entre 0 et 1023, donc son écriture binaire comporte au plus
10 bits). Cette méthode de codage est appelé le complément à 2.


# Q 2
10 bits permettent de représenter 1024 valeurs, donc l'intervalle [-5V,5V]
est discrétisé avec 1024 points, soit une précision de 10/1024, environ
égale à 0.01 V.

# Remarque : soit delta = 10/1024, les valeurs discrètes des mesures sont
# les k*delta, avec k entier dans [-512,511].
"""

##### Liaison avec l'ordinateur

# Q 3
# car_read(4) renvoie '+004' (c'est-à-dire la chaîne formée des 4 caractères
# qui suivent les caractères déjà lus 'U003+012').


# Q 4
# Ne pas oublier dans cette fonction de faire la conversion des chaînes de 
# caractères renvoyées par car_read en nombres entiers.
def lecture_trame(x):
    n = int(car_read(3))  # lecture du nombre de blocs de données
    L = [0]*n
    for k in range(n):  # lecture des n blocs
        L[k] = int(car_read(4))  # lecture d'un bloc (4 caractères, signe compris)
    S = int(car_read(4)) # lecture de la somme de contrôle
    return [x,L,S]
    
    
# Q 5
def checkSum(trame):
    x, L, S = trame
    return (sum(L) % 10000) == S


# Q 6
def lecture_intensite():
    while True:
        # on lit des caractères jusqu'à obtenir un 'I'
        while car_read(1) != 'I':
            pass  # mot clé de Python pour un bloc d'instructions vide
        # on lit la trame puis on vérifie la somme de contrôle
        trame = lecture_trame('I')
        if checkSum(trame):
            return trame
        # sinon on passe à l'itération suivante (i.e. on recommence tout).

# N.B. La boucle while True tourne indéfiniment tant qu'elle n'est pas
# interrompue de force (par un break ou un return).
# Ceci nous permet d'attendre l'obtention d'une trame commençant
# par 'I' et contenant une somme de contrôle correcte.

"""
# code simulant la fonction de liaison avec le capteur, 
# à exécuter préalablement si l'on veut tester les fonctions précédentes
import random
def generateur_car():
    entete = ['U', 'I', 'P']
    while True:
        i = random.randrange(3)
        yield entete[i]
        n = random.randrange(1,10)
        nn = '0'*(3-len(str(n))) + str(n)
        for car in nn: yield car
        somme = 0
        for i in range(n):
            x = random.randrange(-999,1000)
            somme += x
            if x >= 0: yield '+'
            else: yield '-'
            x = str(abs(x))
            x = '0'*(3-len(x)) + x
            for car in x: yield car
        somme += int(random.gauss(.5,.5))  # on introduit une erreur avec
                # une faible probabilité
        somme = str(somme % 10000)
        somme = '0'*(4-len(somme)) + somme
        for car in somme: yield car
        
capteur = generateur_car()

def car_read(nb):
    return ''.join(next(capteur) for k in range(nb))
"""


################################### 2. Analyse des mesures

#### 2.1 Traitement numérique

# Q 7
# L'intégrale de f sur [a,b] est approchée selon la méthode des trapèzes
# (avec n+1 points) par la somme suivante:
# S = h/2 * somme( (f(a + k*h) + f(a + (k+1)*h)) pour k=0,1,...,n-1 ),
# où h = (b-a)/n est le pas,
# ce qui donne après simplification :
# S = h * (f(a)/2 + f(a+h) + f(a+2h) + ... + f(a+(n-1)h) + f(b)/2).


# Q 8
# si L = [a_0,a_1,...,a_n] alors l'intégrale de f sur [0,T]
# est approchée par T/n * (a_0/2 + a_1 + a_2 + ... + a_(n-1) + a_n/2)
# (a_k étant la valeur de f(k*T/n) et T/n le pas de temps).
# La valeur moyenne est donc approchée par 
# 1/n * (a_0/2 + a_1 + a_2 + ... + a_(n-1) + a_n/2)
# Noter que cette expression ne fait pas intervenir directement T, 
# ni le pas de temps.
def trapezes(L):
    n = len(L) - 1   #  /!\ ici len(L) = n+1
    somme = sum(L) - L[0]/2 - L[-1]/2
    return somme / n


# Q 9
def indicateurs(mesures):
    moy = trapezes(mesures)
    ecarts_quad = [(f-moy)**2 for f in mesures]
    ec_type = trapezes(ecarts_quad)**0.5
    return moy, ec_type


#### 2.2 Validation des mesures

# Q 10
# Paramètres à passer à la fonction simulation : la valeur initiale s0 = s(0)
# et le coefficient k 
# (omega sera considéré comme une variable globale).

# Rappel : si on note dt le pas de temps et t[0],...,t[n] les dates 
# discrétisées (t[i] = i*dt), la méthode d'Euler fournit des approximations
# s[i] des valeurs de la solution exacte aux instants t[i]. Ces 
# approximations sont calculées par la relation de récurrence :
# s[i+1] = s[i] - dt * k/10 * (s[i] - e(t[i])).

import numpy as np

def simulation(s0,k):
    dt = 0.002
    n = 1/dt + 1
    s = [0.0] * n  # ici il y a (1/dt)+1 = 501 valeurs à calculer
    s[0] = s0  # valeur initiale
    for i in range(n - 1):
        s[i+1] = s[i] - dt * k/10 * (s[i] - np.sin(omega*i*dt))
    return s

        
# Q 11
def validation(mesures, eps):
    for k in [0.5, 1.1, 2.0]:
        theoric = simulation(0,k)
        if max([abs(mesures[i]-theoric[i]) for i in range(len(theoric))]) <= eps:
            return True
    return False
                

################################### 3. Bases de données
"""
# Q 12

SELECT nSerie
FROM testfin
WHERE 0.4 <= Imoy AND Imoy <= 0.5


# Q 13
# On fait la jointure des deux tables (selon l'attribut nSerie).

SELECT testfin.nSerie, Imoy, Iec, modele
FROM testfin JOIN production ON testfin.nSerie = production.nSerie
WHERE 0.4 < Imoy AND Imoy < 0.5


# Q 14
# On groupe par modèle les données de la table production. La fonction
# d'agrégation COUNT permet d'obtenir l'effectif de chaque groupe.

SELECT modele, count(*) as nombreValide
FROM production
GROUP BY modele


# Q 15
# première solution : avec une sous requête 

SELECT nSerie, fichierMes
FROM testfin
WHERE nSerie NOT IN (SELECT nSerie FROM production)

# deuxième solution : avec une différence ensembliste
# {imprimantes non validées} = {toutes les imprimantes} \ {imprimantes validées}
# subtilité : les deux tables dont on fait la différence doivent avoir
# exactement les mêmes attributs, ce qui oblige à faire une jointure pour
# ajouter les fichiers de mesures à la deuxième table (celle des imprimantes
# validées).

SELECT nSerie, fichierMes
FROM testfin
EXCEPT
SELECT testfin.nSerie, fichierMes
FROM testfin JOIN production ON testfin.nSerie = production.nSerie

# intérêt : analyser les mesures des imprimantes non validées afin de
# déterminer le problème ayant empêché leur validation (en vue d'y remédier).
"""


################################### 4. Compression d'un fichier texte

#### 4.1 Exemples de codages

# Q 16
# le codage de 'EST' est 1001
# celui de 'ASE' est 1001 aussi
# Inconvénient : ce codage n'est pas injectif, donc il ne peut pas être
# décodé !


# Q 17
# La séquence 101101100000100101111 = 10 11 011 000 001 001 011 11
# code le mot 'RETUOOTE'.
# Ce mot de 8 caractères occupe donc 8 octets = 64 bits en mémoire.
# La séquence codée occupe 21 bits. Taux de compression = 64/21 (à peu près = 3).
# Mais pour que le décodage soit possible, il faut fournir non seulement le
# texte codé '101101100000100101111', mais aussi la correspondance entre
# caractères et codes ('E':11, 'R':10, etc).
# Il faut donc stocker aussi cette correspondance dans le fichier compressé,
# ce qui dégrade le taux de compression. Néanmoins ce codage reste intéressant 
# pour un texte long car dans ce cas la place mémoire occupée pour stocker la
# correspondance entre lettres et codes est négligeable devant la taille du 
# texte compressé.


# Q 18
# Le codage doit être injectif (deux textes distincts doivent être codés par
# des séquences de bits distinctes). 
# Voici une condition suffisante garantissant l'injectivité du code : 
# il n'existe pas deux lettres x1 et x2 telles que le code de x1 soit 
# un préfixe du code de x2.
# Le codage de Q 16 ne remplit pas cette condition (le code de E est 1, préfixe
# du code 10 de A) et il est ambigu. Le codage de Q 17 remplit cette condition.

# Remarques.
# - La condition n'est pas nécessaire, par exemple le codage suivant
# 'A':1, 'B':10, 'C':100
# ne la remplit pas, pourtant il est injectif.
# - la condition ci-dessus permet un décodage facile des séquences de bits.


#### 4.2 Tri des caractères selon leur fréquence

# Q 19
def caracteres_presents(donnees):
    cars = []
    for x in donnees:
        # x est-il déjà dans la liste cars ?
        if not est_present(x,cars):
            cars.append(x)
    return cars
    
def est_present(x,cars):  
    # renvoie True si x figure dans la liste cars, False sinon
    for y in cars:
        if x == y:
            return True
    return False
    
# complexité de est_present : O(m) où m = len(cars) <= len(donnees)
# complexité de caracteres_presents : il y a n = len(donnees) itérations,
# chacune ayant un coût majoré par un O(n), donc la complexité cherchée est
# en O(n^2). 
# Ce coût quadratique est effectivement atteint quand tous les caractères
# de la chaîne donnees sont distincts.


# Q 20
def caracteres_frequences(donnees):
    cars = []
    freq = []
    for x in donnees:
        # x est-il déjà dans la liste cars ?
        i = indice(x,cars)
        if i >= 0:
            freq[i] += 1  # si x est déjà présent, augmenter de 1 sa fréquence
        else:
            cars.append(x)  # sinon ajouter x avec une fréquence de 1.
            freq.append(1)
    return cars, freq
    
def indice(x,cars):
    # ici la fonction auxiliaire renvoie un indice i tel que cars[i] = x
    # et si x n'est pas présent dans cars, elle renvoie -1.
    for i in range(len(cars)):
        if x == cars[i]:
            return i
    return -1


# Q 21
# exécution de aux(cars,freq,2,7)
# on indique les valeurs des variables après chaque itération de la boucle while
# (1ere ligne = état initial)
# k m  cars                                     freq
# 7 7 [' ', 'a', 'c', 't', 'e', 'è', 's', 'r'] [4, 6, 3, 4, 8, 1, 7, 4] (ini)
# 6 6 [' ', 'a', 'c', 't', 'e', 'è', 's', 'r'] [4, 6, 3, 4, 8, 1, 7, 4] (1e iter)
# 5 6 [' ', 'a', 'c', 't', 'e', 'è', 's', 'r'] [4, 6, 3, 4, 8, 1, 7, 4] (2e iter)
# 4 5 [' ', 'a', 'c', 't', 'è', 'e', 's', 'r'] [4, 6, 3, 4, 1, 8, 7, 4]
# 3 5 [' ', 'a', 'c', 't', 'è', 'e', 's', 'r'] [4, 6, 3, 4, 1, 8, 7, 4]
# 2 5 [' ', 'a', 'c', 't', 'è', 'e', 's', 'r'] [4, 6, 3, 4, 1, 8, 7, 4]
# après la sortie de la boucle, on échange les éléments d'indices 5 et 7,
# d'où l'état final des deux tableaux :
# [' ', 'a', 'c', 't', 'è', 'r', 's', 'e'] [4, 6, 3, 4, 1, 4, 7, 8]
# et la valeur renvoyée est 5.


# Q 22
# La fonction aux travaille sur le sous-tableau allant des indices i à j 
# (inclus).
# Il s'agit d'une fonction de partition (servant à effectuer un tri rapide),
# elle utilise comme pivot le dernier élément du sous-tableau : pivot = freq[j], 
# et la partition se fait à partir de la droite.
# La fonction partitionne le tableau [freq[i],...,freq[j]] de façon à placer
# à gauche du pivot les éléments <= pivot et à droite les éléments > pivot.

# Preuve formelle :
# - terminaison : à chaque itération de la boucle while, k diminue de 1
# donc la boucle termine en exactement j-i itérations (on aurait aussi bien
# pu faire une boucle for).
# - invariant : un dessin vaut mieux qu'un long discours
#           i               k            m            j
# ----------------------------------------------------------- 
#          |      ...      |  <= pivot  |  > pivot   |p|
# -----------------------------------------------------------
# (p désigne le pivot, les points de suspension les éléments pas encore
# examinés).
# A la fin de chaque itération de la boucle while, on a l'invariant suivant :
# freq[r] <= pivot pour tout indice r tel que k <= r < m
# freq[r] > pivot pour tout indice r tel que m <= r < j.
# Cet invariant est vrai avant d'entrer dans la boucle while
# (les 2 conditions sont vides lorsque k = m = j), 
# et il est conservé par les itérations : on décrémente k de 1 et on examine
# freq[k] (qui est alors l'élément le plus à droite de la zone contenant les 
# points de suspension) : s'il est <= pivot il n'y a rien à faire;
# s'il est > pivot (i.e. freq[k] > freq[j]), on l'échange avec l'élément le
# plus à droite de la zone <= pivot : pour cela on décrémente m de 1 et on
# effectue l'échange des éléments d'indices k et m. Ainsi l'invariant est
# maintenu.
# En sortie de la boucle while, on a k = i, et on effectue l'échange du pivot
# avec l'élément d'indice m, afin de mettre le pivot à sa place définitive
# entre les 2 zones;
#           i                m                       j
# ----------------------------------------------------------- 
#          |    <= pivot    |p|        > pivot        |
# -----------------------------------------------------------
# La fonction aux a donc bien réalisé la partition du sous tableau
# [freq[i],...,freq[j]] autour du pivot p.

# Quant au tableau cars, il est modifié en même temps que freq : chaque fois
# qu'on échange deux éléments de freq, on echange les deux mêmes éléments
# de cars, afin de conserver la propriété de correspondance entre les deux
# tableaux :
# pour tout indice r, freq[r] est le nombre d'occurrences de cars[r] dans
# le texte initial.


# Q 23
# L'instruction suivante effectue le tri souhaité :
# >>> tri(cars,freq,0,len(cars)-1)
# La fonction tri effectue le tri rapide (quicksort) à l'aide de la fonction
# de partition aux (le tableau freq est trié en ordre croissant et le tableau
# cars est modifié au fur et à mesure afin de conserver la correspondance entre
# les deux tableaux).
# La complexité dans le pire cas du tri rapide est O(n^2), où n est la 
# longueur des tableaux considérés (mais en moyenne il est bien meilleur).

# Le tri par fusion a une complexité plus faible dans le pire cas : O(n*ln(n)).


#### 4.3 Codage de Huffman

# Q 24
def insertion(t,q,cars_ponderes):
    i = 0
    while i < len(cars_ponderes) and cars_ponderes[i][1] < q:
        i += 1
    # cars_ponderes = cars_ponderes[:i] + [[t,q]] + cars_ponderes[i:]
    # ou mieux :
    cars_ponderes.insert(i, [t,q])
    return cars_ponderes


# Q 25
def codage(cars,freq):
    # cars et freq sont triés par fréquence croissante
    cars_ponderes = [[cars[i], freq[i]] for i in range(len(cars))]
    # la liste des cars_ponderes restera triée par poids croissant au cours
    # de l'algorithme.
    code = [''] * len(cars)  # initialisation du tableau des codes
    # le code de chaque caractère x est code[k] où k = numero(x)
    while len(cars_ponderes) > 1: 
        [s1, p1] = cars_ponderes[0]
        [s2, p2] = cars_ponderes[1]
        s = s1 + s2  # concaténation des chaînes
        p = p1 + p2  # somme des poids
        cars_ponderes = cars_ponderes[2:]  # suppression de [s1,p1] et [s2,p2]
        cars_ponderes = insertion(s, p, cars_ponderes)  # insertion de [s,p]
        # ajout d'un 0 dans les codes des lettres de s1
        for x in s1:  # x est un caractère
            k = numero(x)  # le code de x est code[k]
            code[k] = '0' + code[k]
        for x in s2:  # idem avec s2
            k = numero(x) 
            code[k] = '1' + code[k]
    return code

"""# exemple
def numero(x): return ord(x)-ord('a')  # envoie 'a' sur 0, 'b' sur 1, etc.

codage(['f','b','d','a','c','e'], [1, 2, 3, 4, 4, 9])
# on obtient ['110', '1001', '111', '101', '0', '1000']
"""


#### 4.4 Décodage

# Q 26
# '0' < '1000' < '1001' < '101' < '110' < '111'


# Q 27
# décodage de txt_bin = '010100000011011101111011110'
# voici le découpage :
# txt_bin = '010' + '10' + '000' + '001' + '10' + '1110' + '1111' + '011' + '110'
# à savoir :
# pos = 0, m = 2, code '010', lettre 'b'
# pos = 3, m = 4, code '10', lettre 'o'
# pos = 5, m = 0, code '000', lettre 'n'
# pos = 8, m = 1, code '001', lettre 'j'
# pos = 11, m = 4, code '10', lettre 'o'
# pos = 13, m = 6, code '1110', lettre 'u'
# pos = 17, m = 7, code '1111', lettre 'r'
# pos = 21, m = 3, code '011', lettre ' '
# pos = 24, m = 5, code '110', lettre '!'
# le texte ayant été compressé était donc 'bonjour !'


# Q 28
def decode_car(txt_bin,pos,code):
    # recherche dichotomique d'un préfixe de txt_bin[pos:] dans le tableau de
    # chaînes code (trié par ordre lex)
    g = 0
    d = len(code) - 1
    while g <= d:
        m = (g + d) // 2
        long = len(code[m])  # longueur de la sous-chaîne à tester
        if txt_bin[pos:pos+long] == code[m]:  # pas d'erreur même si pos+long>len(txt_bin)
            return m
        elif txt_bin[pos:pos+long] < code[m]:  # comparaison pour ordre lex
            d = m - 1
        else:
            g = m + 1
    # normalement on ne sort pas du while sans avoir trouvé ce qu'on cherche
    # sinon il y a un souci dans le codage !
    print("erreur de décodage")

# C'est exactement une recherche dichotomique dans un tableau trié (le tableau
# code), sauf qu'au lieu de manipuler des nombres ordonnés dans l'ordre usuel
# on manipule des chaînes de caaractères, ordonnées par ordre lexicographique.
# Subtilité technique : les codes ayant des longueurs variables, au moment
# de tester si code[m] est présent dans la chaîne txt_bin à la position pos,
# il faut extraire de txt_bin une sous_chaine de longueur adéquate 
# (la variable long sert à cela).

# Le nombre de comparaisons de chaînes est du même ordre de grandeur que le
# nombre d'itérations de la boucle while (il y a une ou deux comparaisons à
# chaque itération, dans le if et le elif), donc c'est en O(log n) où
# n = len(code)
#
# Rappel de la preuve : à chaque itération on travaille dans le sous-tableau 
# [code[g],...,code[d]], de longueur d-g+1; initialement la longueur est n
# et à chaque itération cette longueur est au moins divisée par 2;
# dans le pire cas le nombre p d'itérations vérifie n ~= 2^p, soit
# p ~= log(n).


# Q 29
def decode_txt(txt_bin,code,cars):
    txt_clair = ""
    pos = 0
    while pos < len(txt_bin):
        m = decode_car(txt_bin,pos,code)  # décoder un caractère
        txt_clair += cars[m]  # ajouter ce caractère dans le texte décodé
        pos += len(code[m])  # passer à la position du caractère codé suivant
    return txt_clair

"""# exemple
txt_bin = '010100000011011101111011110'
code= ['000', '001', '010', '011', '10', '110', '1110', '1111']
cars = ['n', 'j', 'b', ' ', 'o', '!', 'u', 'r']
decode_txt(txt_bin,code,cars)  # renvoie 'bonjour !'
"""

