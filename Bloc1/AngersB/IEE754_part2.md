# IEEE754 : partie II



### Lecture rapide des flottants

La machine traite une chaîne de bits `s e f` ainsi:



| e        | f      |     $`\rightarrow`$          | v|
|:-        |:-     |  :-:          | :-                  |
|$`e=0...0`$ | $`f=0`$ | $`\rightarrow`$ | $`v=(-1)^s\times 0.0`$|
$`e=0...0`$ | $`f\neq  0`$ | $`\rightarrow`$ | $`v=(-1)^s\times  0.f \times 2^{1-E_{\max}}`$|
$`e=1...1`$ | $`f=  0`$ | $`\rightarrow`$ | $`v=(-1)^s\times \infty`$|
$`e=1...1`$ | $`f\neq 0`$ | $`\rightarrow`$ | $`\text{NaN}`$|
$`0...0<e<1...1`$ | $`f\neq  0`$ | $`\rightarrow`$ | $`v=(-1)^s\times  1.f \times 2^{e-E_{\max}}`$|



### Tableau récapitulatif
On notera $`ε_m=2^{-n}`$ l'*epsilon* de  la machine, c'est-à-dire le successeur de 1.

On notera $`λ`$ le plus petit VF normal positif.

On notera $`μ`$ le plus petit VF sous-normal positif.

On notera $`Ω`$ le plus grand VF normal.

> **Recherche**
>  Quellle relation existe-t-il entre $`μ`$, $`ε_m`$ et $`λ`$?


|    Format | $`\# E`$ | $`\# f`$ | $`E_{\min}`$ | $`E_{\max}`$ | $`ε_m`$ | $`λ`$ | $`μ`$ | $`Ω`$ |
|--|--|--|--|--|--|--|--|--|
|`toy7` | 3 | 3 | $`-2`$| $`+3`$ | $`2^{-3}=1/8`$ | $`2^{-2}=1/4`$ | $`2^{-5}=1/32`$ | $`1,111\times 2^3=15`$ |
|`binary32` | 8 | 23 | $`-126`$ |$`+127`$ |$`2^{-23}\approx 1,2\times 10^{-7}`$ |$`2^{-23}\approx 1,2\times 10^{-7}`$ |$`2^{-23}\approx 1,2\times 10^{-7}`$| $`2^{-23}\approx 1,2\times 10^{-7}`$|
|`binary64` | 11 | 52 | $`-1022`$| | | | | 





### Comparaison

>Il  est  très  simple  et  rapide  de  comparer  deux  VF:  comment  la  machine procède-t-elle? Quel est l'avantage de ce stockage des VF?


### Addition

On décompose l'action en trois étapes:


1. on commence par ramener les deux nombres au même exposant, en l'occurrence
  le plus grand des deux;
2. on ajoute les deux mantisses *complètes* en tenant compte du signe;
3. on renormalise le nombre obtenu.




Par exemple, en `toy7`, additionnons 1,1 et 0,0111 ou plutôt $`1,1\times
2^{0}`$  et  $`1,11\times  2^{-2}`$  ou  plutôt  (en `toy7`,  le  décalage
d'exposant est de 3):



 1,1: `0  011  100`
 0,0011: `0 001 110`


On réécrit 0,00101 avec  le même exposant que 1,1 mais  attention à la précision
de la mantisse!

1,100

0,001**11**

------------

1,101**11**


On ne peut pas garder les deux derniers 1 du plus petit nombre car on n'a pas la place de
les stocker!

Que faire? Il faut arrondir en choisissant selon quatre méthodes usuelles:


> **Les arrondis**
>
>  1. l'arrondi au plus  proche (RN) qui arrondit au VF...le  plus proche.  En cas
>     d'égalité, on choisit la valeur paire (donc qui se termine par un 0 en binaire); 
>  2. l'arrondi vers 0 (RZ) qui arrondit à la valeur de plus petite valeur 
>     absolue: c'est la troncature; 
>  3. l'arrondi vers $`+\infty`$ (RU) qui  arrondit à la valeur supérieure la plus   petite;
>  4. l'arrondi vers $`-\infty`$ (RD) qui arrondit à la valeur inférieure la plus grande.


Le mode d'arrondi par défaut est le premier.




Posons $`x=1,10111`$  (qui n'est  pas un VF!):  il est compris  entre les  deux VF
$1,101`$ et $`1,110$.

$`
1,101\textcolor{blue}{ \mathit{00}} \leqslant \underbrace{1,101\textcolor{blue}{ \mathit{11}}}_x \leqslant 1,110\textcolor{blue}{ \mathit{00}}
`$

$`
\underbrace{1,100+ \textcolor{blue}{\mathit{1,00}}ulp(x)}_v \leqslant  \underbrace{1,100+ \textcolor{blue}{ \mathit{1,11}}ulp(x)}_{x} \leqslant \underbrace{1,100+ \textcolor{blue}{\mathit{10,00}}ulp(x)}_{\operatorname{succ}(v)}
`$

$`|x-v|=0,11ulp(x) `$,  $`\ |x- \operatorname{succ}(v)|=0,01ulp(x)`$  donc $`RN(x)=\operatorname{succ}(v)`$


![ulp](./ulp.png)

$`RN(1,1 + 0,001) = 1,110`$


## D'autres exercices

### Dérivées

Que pensez-vous de ça :
```python
from math import sin, pi
def der(f,h) :
    return lambda x: (f(x + h) - f(x)) / h

def opp(f) :
    return lambda x : -f(x) 

cos_app   = lambda h : der(sin, h)
sin_app   = lambda h : opp(der(cos_app(h), h))
cos_app_2 = lambda h : der(sin_app(h), h)
sin_app_2 = lambda h : opp(der(cos_app_2(h), h))
```


Qui donne:

```python
In [94]: sin(pi / 2)
Out[94]: 1.0

In [95]: sin_app(1e-7)(pi / 2)
Out[95]: 0.9992007221626409

In [96]: sin_app_2(1e-7)(pi / 2)
Out[96]: -1110223024625.1565
```


### Pourquoi `3 * 0.1 != 0.3` ? 
  Commentez le code suivant sachant que la fonction 
`ieee(x, garde,  l_pow, l_mant)` renvoie l'écriture  IEEE 754
du flottant  `x` avec un certain  nombre de bits de  garde et
un format de mantisse et d'exposant:


```python
In [201]: ieee(0.1,3,11,52)
normal
ulp = 2**-56
les bits oubliés sont 10
Out[201]: '0 01111111011 1001100110011001100110011001100110011001100110011010'

In [202]: ieee(2*0.1,3,11,52)
normal
ulp = 2**-55
les bits oubliés sont 10
Out[202]: '0 01111111100 1001100110011001100110011001100110011001100110011010'

In [203]: ieee(3*0.1,3,11,52)
normal
ulp = 2**-54
les bits oubliés sont 00
Out[203]: '0 01111111101 0011001100110011001100110011001100110011001100110100'

In [204]: ieee(0.3,3,11,52)
normal
ulp = 2**-54
les bits oubliés sont 00
Out[204]: '0 01111111101 0011001100110011001100110011001100110011001100110011'

In [205]: ieee(4*0.1,3,11,52)
normal
ulp = 2**-54
les bits oubliés sont 10
Out[205]: '0 01111111101 1001100110011001100110011001100110011001100110011010'

In [206]: ieee(0.4,3,11,52)
normal
ulp = 2**-54
les bits oubliés sont 10
Out[206]: '0 01111111101 1001100110011001100110011001100110011001100110011010'

In [207]: ieee(0.5,3,11,52)
normal
ulp = 2**-53
les bits oubliés sont 00
Out[207]: '0 01111111110 0000000000000000000000000000000000000000000000000000'

In [208]: ieee(5*0.1,3,11,52)
normal
ulp = 2**-53
les bits oubliés sont 00
Out[208]: '0 01111111110 0000000000000000000000000000000000000000000000000000'
```


### Bits de garde, d'arrondi, collant
  Soustrayez $`1,101\times 2^{-2}`$ puis $`1,111\times 2^{-2}`$ à $`1\times 2^0`$ en `toy7` en gardant tous les bits, même ceux qui dépassent le format de la mantisse puis refaites le calcul en faisant disparaître les bits qui dépassent de la mantisse.

Pourquoi parle-ton de *bit de garde*?

En fait, il y a toujours trois bits à droite de la mantisse: G le bit de garde (*guard*), R le bit d'arrondi (*rounding*) et S le bit collant (*sticky*).

Les deux  premiers sont comme  des paniers  qui recueillent les  bits translatés (*shifted*). Le dernier  est un panier absorbant  qui est un ou  inclusif des bits translatés au-delà de R.
